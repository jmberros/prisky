from setuptools import setup, find_packages

setup(
    name="prisky",
    version="0.1",
    description="Polygenic Risk Scores for everyone",
    url="https://gitlab.com/jmberros/prisky",
    author="Juan Manuel Berros",
    author_email="juanma.berros@gmail.com",
    license="MIT",
    packages=find_packages(),
    include_package_data=True,
    entry_points="""
        [console_scripts]
        prisky=prisky.cli:cli
    """
)
