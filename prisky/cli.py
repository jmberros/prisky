import click
import click_didyoumean

# from prisky.logger import logger


TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
PRISKY_LOGO = r"""

   ██████╗ ██████╗ ██╗███████╗██╗  ██╗██╗   ██╗
   ██╔══██╗██╔══██╗██║██╔════╝██║ ██╔╝╚██╗ ██╔╝
   ██████╔╝██████╔╝██║███████╗█████╔╝  ╚████╔╝
   ██╔═══╝ ██╔══██╗██║╚════██║██╔═██╗   ╚██╔╝
   ██║     ██║  ██║██║███████║██║  ██╗   ██║
   ╚═╝     ╚═╝  ╚═╝╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝

"""


@click.group(cls=click_didyoumean.DYMGroup)
def cli():
    click.echo(PRISKY_LOGO, err=True)
    # now_pretty = datetime.now().strftime(TIME_FORMAT)
    # logger.info(f'---\nStarting prisky at {now_pretty}\n')

cli.name = 'prisky'

from prisky.commands import *
