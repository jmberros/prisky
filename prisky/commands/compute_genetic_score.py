import re
from glob import glob

from prisky.cli import cli
from prisky.logger import logger, set_logfile, log_command_name_and_args
from prisky.helpers import run_shell_command, abs_label, check_output, \
    check_input
from .command_options import label_option, path_to_plink_option


@cli.command()
@label_option
@path_to_plink_option
def compute_genetic_score(label, path_to_plink):
    '''
    Search for a plink dataset at {label}.(bed|bim|fam) and use it
    to compute genetic scores with each of the {label}.*.scores.tsv
    files found.
    '''
    plink_label = abs_label(label)
    score_files = glob(f'{plink_label}.*.scores.tsv')
    set_logfile(abs_label(label) + '.compute_genetic_score.log')
    log_command_name_and_args()

    check_input(plink_label + '.bed')
    check_input(plink_label + '.bim')
    check_input(plink_label + '.fam')

    for scores_file in score_files:
        score_tag = re.search(rf'{plink_label}\.(.*)\.scores.tsv',
                              scores_file).group(1)
        profile_file = f'{plink_label}.{score_tag}.profile'

        if check_output(profile_file):
            continue

        command = (f'{path_to_plink} --bfile {plink_label} ' +
                   f'--score {scores_file} sum ' +
                   f'--out {plink_label}.{score_tag}')
        run_shell_command(command)
        logger.info(f'\nDone:\n  {profile_file}\n')
