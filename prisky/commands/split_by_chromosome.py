import pandas as pd

from prisky.cli import cli
from prisky.helpers import check_input, run_shell_command, Timer
from prisky.logger import set_logfile, logger, log_command_name_and_args
from .command_options import label_option, path_to_plink_option


@cli.command()
@label_option
@path_to_plink_option
def split_by_chromosome(label, path_to_plink):
    '''
    Split a PLINK dataset by chromosome and put the results in a subdirectory.
    '''
    set_logfile(f'{label}.split_by_chromosome.log')
    log_command_name_and_args()
    timer = Timer()

    check_input(f'{label}.bed')
    check_input(f'{label}.bim')
    check_input(f'{label}.fam')

    # Crear una carpeta para los chrom files
    chromosomes_dir = label.parent/f'{label.name}_chrom_files'
    command = f'mkdir -p {chromosomes_dir}'
    run_shell_command(command)

    # Leer el BIM para ver qué cromosomas hay
    bim_colnames = ['chrom', 'id', 'cM', 'pos', 'A1', 'A2']
    variants = pd.read_csv(f'{label}.bim', sep=r'\s+', names=bim_colnames)
    chromosomes_in_dataset = variants['chrom'].astype(str).unique()

    logger.info('Found these chromosomes in the dataset: ' +
                ', '.join(chromosomes_in_dataset) + '\n')

    # Llamar a PLINK por cada cromosoma
    for chrom in chromosomes_in_dataset:
        chrom_label = chromosomes_dir/f'{label.name}.chr{chrom}'
        command = (
            f'{path_to_plink} --bfile {label} ' +
            f'--chr {chrom} --make-bed --out {chrom_label}'
        )
        run_shell_command(command)

    logger.info('Done! Created PLINK datasets for ' +
                f'{len(chromosomes_in_dataset)}' +
                f' chromosomes. Check the directory: {chromosomes_dir}')

    timer.stop_and_log()
