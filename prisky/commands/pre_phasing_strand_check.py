from prisky.cli import cli
from prisky.helpers import run_shell_command, find_chromosome_datasets, Timer, \
    write_commands_to_file
from prisky.logger import set_logfile, log_command_name_and_args
from .command_options import label_option, path_to_shapeit_option, \
    genetic_maps_dir_option, reference_dir_option, only_chrom_option, \
    path_to_parallel_option, threads_option


@cli.command()
@label_option
@path_to_shapeit_option
@path_to_parallel_option
@genetic_maps_dir_option
@reference_dir_option
@only_chrom_option
@threads_option
def pre_phasing_strand_check(label, path_to_shapeit, path_to_parallel, threads,
                             genetic_maps_dir, reference_dir, only_chrom=None):
    '''
    Do a strand check of chromosome datasets before phasing, using shapeIT.
    '''
    set_logfile(f'{label}.phase_strand_check.log')
    log_command_name_and_args()

    timer = Timer()

    map_template = str(genetic_maps_dir/'genetic_map_chrN_combined_b37.txt')
    hap_template = str(reference_dir/'1000GP_Phase3_chrN.hap.gz')
    legend_template = str(reference_dir/'1000GP_Phase3_chrN.legend.gz')
    ref_sample_file = reference_dir/'1000GP_Phase3.sample'

    chrom_datasets = find_chromosome_datasets(label, exit_if_zero=True,
                                              only_chrom=only_chrom)

    commands = []
    for chrom_number, chrom_dataset_prefix in chrom_datasets:
        chrom_map = map_template.replace('N', chrom_number)
        chrom_hap = hap_template.replace('N', chrom_number)
        chrom_legend = legend_template.replace('N', chrom_number)
        command = (
            f'{path_to_shapeit} -check ' +
            f'--input-bed {chrom_dataset_prefix} ' +
            f'--input-map {chrom_map} ' +
            f'--input-ref {chrom_hap} {chrom_legend} {ref_sample_file} ' +
            f'--output-log {chrom_dataset_prefix}.alignments'
        )
        commands.append(command)

    commands_file = f'{label}.pre-phasing-strand-check_commands.list'
    write_commands_to_file(commands, commands_file)

    n_jobs = min(threads, len(commands))
    parallel_command = f'{path_to_parallel} --jobs {n_jobs} --verbose -- < {commands_file}'
    run_shell_command(parallel_command)

    timer.stop_and_log()
