from prisky.cli import cli
from prisky.logger import set_logfile, log_command_name_and_args, logger
from prisky.helpers import run_shell_command, package_root, abs_label
from .command_options import label_option, path_to_rscript_option


@cli.command()
@label_option
@path_to_rscript_option
def plot_scores(label, path_to_rscript):
    '''
    Plot the scores distribution per population.
    '''
    dataset_label = abs_label(label)
    set_logfile(dataset_label + '.plot_scores.log')
    log_command_name_and_args()
    logger.info('Run a shiny App')

    app_dir = package_root/'priskyr'
    server_script = app_dir/'cli.R'
    command = f'{path_to_rscript} {server_script} ' + \
              f'--app-dir {app_dir} ' + \
              f'--label {dataset_label} ' + \
              '--port 7575 ' + \
              '--host 0.0.0.0'
    run_shell_command(command)
