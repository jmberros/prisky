import psutil
from collections import Counter
from glob import glob

import pandas as pd
from humanfriendly import format_size
from termcolor import colored

from prisky.cli import cli
from prisky.helpers import quit_if_output_ready, abs_label
from prisky.logger import logger, set_logfile, log_command_name_and_args
from .command_options import label_option


def find_UKBB_assoc_tsv(label):
    '''
    Given a label like path/to/UKBB-Pheno, find a single association TSV file
    like path/to/UKBB-Pheno.43189.assoc.tsv.
    '''
    tsv_files = glob(f'{abs_label(label)}.*.assoc.tsv')
    if not len(tsv_files):
        logger.error(f'No assoc TSV found for label {label}')
        raise NoAssocFileException
    elif len(tsv_files) > 1:
        logger.error(f"{len(tsv_files)} assoc TSV files found, I don't " +
                     'know which one to choose 😳')
        raise ManyAssocFilesException
    return tsv_files[0]

class NoAssocFileException(Exception): pass
class ManyAssocFilesException(Exception): pass


@cli.command()
@label_option
def prepare_UKBB_tsv(label):
    '''
    Parse a file of UKBB associations to format it as a TSV that can be used
    by prisky's pipeline.
    '''
    dest_tsv = abs_label(label) + '.tsv'
    set_logfile(dest_tsv + '.log')
    log_command_name_and_args()
    quit_if_output_ready(dest_tsv)

    logger.info(f'No {dest_tsv} file found. Assuming this is a UKBB dataset.')
    raw_tsv = find_UKBB_assoc_tsv(label)

    free_memory = psutil.virtual_memory().available
    logger.info(
        f'Reading associations from: {raw_tsv}. ' +
        colored('This might take ~2GB of RAM depending on the dataset! ', 'red') +
        colored(f'Free memory: {format_size(free_memory)}', 'green')
    )

    # UKBB has big datasets, so the usecols option alleviates some of the
    # loading:
    raw_df = pd.read_csv(raw_tsv, sep=r'\s+',
                         usecols=['variant', 'rsid', 'beta', 'pval'])
    logger.info(f'Found {len(raw_df)} associations')
    df = keep_significant_variants(raw_df)
    logger.info(f'{len(df)} significant associations kept')
    df = remove_multiline_variants(df)
    logger.info(f'{len(df)} associations after removing multiline variants')
    df = remove_nonrsid_variants(df)
    logger.info(f'{len(df)} associations after removing non-RSID variants')
    df['effect_allele'] = df['variant'].map(extract_effect_allele)
    df = df.rename(columns={
        'rsid': 'variant_id',
        'beta': 'basic_score',
        'pval': 'p_value',
    })

    df = df[['variant_id', 'effect_allele', 'basic_score', 'p_value']]
    logger.info(f'Write the result to: {dest_tsv}')
    df.to_csv(dest_tsv, sep='\t', index=False)


def remove_multiline_variants(df):
    '''
    Remove the rows that have an rsid which appears multiple times in the
    dataset (rationale: this removes the non-biallelic SNPs).
    '''
    rsid_counts = Counter(df['rsid'])
    dupe_rsids = {rsid for rsid, count in rsid_counts.items() if count > 1}
    is_dupe = df['rsid'].isin(dupe_rsids)
    return df[~is_dupe].reset_index(drop=True)


def keep_significant_variants(df):
    '''
    Keep the rows with a significant p-value.
    '''
    threshold = 0.001
    logger.info(f'Using p-value threshold of {threshold}')
    is_significant = df['pval'] <= threshold
    return df[is_significant].reset_index(drop=True)


def extract_effect_allele(variant_ukbb_name):
    '''
    Extract the effect allele (always the ALT/last one mentioned) from a UKBB
    dataset variant name like '1:1000:A:T' => 'T'.
    '''
    return variant_ukbb_name.split(':')[-1]


def remove_nonrsid_variants(df):
    '''
    Remove the variants with a 'rsid' that is not an actual rs ID.
    '''
    well_formatted_rsid = df['rsid'].str.match(r'^rs[0-9]+$')
    return df[well_formatted_rsid].reset_index(drop=True)
