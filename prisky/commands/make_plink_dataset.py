from prisky.cli import cli
from prisky.logger import set_logfile, log_command_name_and_args
from prisky.helpers import quit_if_output_ready, run_shell_command, abs_label, \
    check_input
from .command_options import label_option, path_to_plink_option


@cli.command()
@label_option
@path_to_plink_option
def make_plink_dataset(label, path_to_plink):
    '''
    Make a plink dataset {label}.{bed,bim,fam} from the {label}.vcf file.
    '''
    input_vcf = abs_label(label) + '.vcf.gz'
    out_label = abs_label(label)
    set_logfile(out_label + '.make_plink_dataset.log')
    log_command_name_and_args()
    check_input(input_vcf)
    quit_if_output_ready(f'{out_label}.bed')

    command = f'{path_to_plink} --vcf {input_vcf} --make-bed --out {out_label}'
    run_shell_command(command)
