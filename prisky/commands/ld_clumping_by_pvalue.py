import click

from prisky.cli import cli
from prisky.logger import logger, set_logfile, log_command_name_and_args
from prisky.helpers import run_shell_command, abs_label, check_output, \
    check_input
from .command_options import label_option, path_to_plink_option

def ld_clumping_options(func):
    func = click.option('--clump-snp-field', default='variant_id')(func)
    func = click.option('--clump-field', default='p_value')(func)
    func = click.option('--clump-p1', default=0.0001)(func)
    func = click.option('--clump-p2', default=0.01)(func)
    func = click.option('--clump-kb', default=250)(func)
    func = click.option('--clump-r2', default=0.5)(func)
    return func

@cli.command()
@ld_clumping_options
@label_option
@path_to_plink_option
def clump_by_pvalue(label, path_to_plink, **clump_args):
    '''
    Clump the variants of a PLINK dataset based on linkage disequilibrium and
    for each clump keep the one with the smallest p-value. Produces a .clumped
    file.

    From the PLINK docs:

        Clumps are formed around central 'index variants' which, by default,
        must have p-value no larger than 0.0001; change this threshold with
        --clump-p1. Index variants are chosen greedily starting with the lowest
        p-value. Variants which meet the --clump-p1 threshold, but have already
        been assigned to another clump, do not start their own clumps unless
        --clump-best was specified

        https://www.cog-genomics.org/plink/1.9/postproc#clump
    '''
    dataset_tag = abs_label(label)
    clumped_file = dataset_tag + '.clumped'
    set_logfile(clumped_file + '.log')
    log_command_name_and_args()

    check_input(dataset_tag + '.bed')
    check_input(dataset_tag + '.bim')
    check_input(dataset_tag + '.fam')

    if not check_output(clumped_file):
        run_shell_command(
            (f'{path_to_plink} ' +
            f'--bfile {dataset_tag} ' +
            f'--clump {dataset_tag}.tsv ' +
            '--clump-p1 {clump_p1} --clump-p2 {clump_p2} ' +
            '--clump-field {clump_field} ' +
            '--clump-snp-field {clump_snp_field} ' +
            '--clump-kb {clump_kb} ' +
            '--clump-r2 {clump_r2} ' +
            f'--out {dataset_tag}')
            .format(**clump_args)
        )

    prune_file = dataset_tag + '.prune.in'
    if not check_output(prune_file):
        run_shell_command(
            # The third column of the .clumped file contains the SNP IDs
            f"awk '{{ print $3 }}' {dataset_tag}.clumped | " +
            f"grep -E '^rs[0-9]+$' > {prune_file}"
        )

    pruned_set_tag = dataset_tag + '.ld-pruned'
    if not check_output(pruned_set_tag + '.bed'):
        run_shell_command(
            f'{path_to_plink} ' +
            f'--bfile {dataset_tag} ' +
            f'--extract {prune_file} ' +
            f'--make-bed --out {pruned_set_tag}'
        )

    # We will also produce a new TSV of variants keeping only the set of
    # independent variants, for later analyses.

    variants_tsv = dataset_tag + '.tsv'
    variants_pruned_tsv = pruned_set_tag + '.tsv'
    if not check_output(variants_pruned_tsv):
        # First extract the header of the TSV:
        run_shell_command(f'head -n1 {variants_tsv} > {variants_pruned_tsv}')
        # Then append the matching variants:
        run_shell_command(
            f'grep -wf {prune_file} {variants_tsv} >> {variants_pruned_tsv}'
        )
