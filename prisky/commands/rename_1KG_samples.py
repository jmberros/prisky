from os.path import isfile

import pandas as pd

from prisky.cli import cli
from prisky.logger import logger, set_logfile, log_command_name_and_args
from prisky.helpers import run_shell_command, abs_label, get_1KG_samples, \
    check_input
from .command_options import label_option, path_to_bcftools_option


@cli.command()
@label_option
@path_to_bcftools_option
def rename_1KG_samples(label, path_to_bcftools):
    '''
    Change the 1KG sample names in a VCF to include their population/region,
    for instance `HG00096` to `HG00096-GBR-EUR`.
    '''
    dataset_label = abs_label(label)
    in_vcf = f'{dataset_label}.1KG.vcf.gz'
    out_vcf = f'{dataset_label}.1KG.samples_renamed.vcf.gz'
    set_logfile(f'{out_vcf}.log')
    log_command_name_and_args()
    check_input(in_vcf)

    if not isfile(out_vcf):
        dataset_sample_ids = read_samples_from_vcf(in_vcf, path_to_bcftools)
        new_sample_ids = make_new_sample_ids(dataset_sample_ids)
        samples_file = f'{dataset_label}.samples_renaming.tsv'
        make_samples_file(dataset_sample_ids, new_sample_ids, samples_file)
        rename_samples_in_vcf(in_vcf, samples_file, out_vcf, path_to_bcftools)

    logger.info(f'Output ready: {out_vcf}')


def rename_samples_in_vcf(in_vcf, samples_file, out_vcf, path_to_bcftools):
    '''
    Rename the samples in a VCF (*in_vcf*) with the names in *samples_file*
    using *path_to_bcftools* and write a new VCF in *out_vcf*.
    '''
    command = f'{path_to_bcftools} reheader --samples {samples_file} {in_vcf}'
    command += f' > {out_vcf}'
    run_shell_command(command)


def make_samples_file(old_sample_ids, new_sample_ids, filepath):
    '''
    Make a tab-separated file of sample names / new sample names for bcftools.
    '''
    df = pd.DataFrame({'old_ids': old_sample_ids, 'new_ids': new_sample_ids})
    df[['old_ids', 'new_ids']].to_csv(filepath, sep='\t', header=None, index=False)


def read_samples_from_vcf(vcf, path_to_bcftools):
    lines = run_shell_command(f'{path_to_bcftools} query --list-samples {vcf}',
                              silence_stdout=True)
    return [line.strip() for line in lines]


def make_new_sample_ids(sample_ids):
    samples_1KG = get_1KG_samples()
    present_in_dataset = samples_1KG['sample'].isin(sample_ids)
    dataset_samples = samples_1KG[present_in_dataset].reset_index()
    def make_id(sample_row):
        return '{sample}-{pop}-{super_pop}'.format(**sample_row)
    return list(dataset_samples.apply(make_id, axis=1).values)
