import click

from prisky.cli import cli
from prisky.logger import set_logfile, log_command_name_and_args
from prisky.helpers import run_shell_command, check_input, R_dir
from .command_options import path_to_plink_option, path_to_rscript_option


@cli.command()
@click.option('--prefix-own', required=True)
@click.option('--prefix-ref', required=True)
@click.option('--prefix-out', required=True)
@path_to_rscript_option
@path_to_plink_option
def merge_datasets_fixing_strand(path_to_rscript, path_to_plink,
                                 prefix_own, prefix_ref, prefix_out):
    '''
    Merge two plink datasets (own and ref) doing this:

      (1) Leave out variants with unknown alleles.
      (2) Leave out variants with ambiguous strand.
      (3) Leave out variants with non matching positions.
      (4) Fix the strand of the alleles to match the reference.

    The result is a new plink dataset (bed, bim, fam).
    '''
    set_logfile(f'{prefix_own}.merge_datasets_fixing_strand.log')
    log_command_name_and_args()

    # Write a new BIM and variants list:
    fix_strand_script = R_dir/'fix_strand_and_filter_bim_using_reference.R'
    prefix_own_subset = f'{prefix_own}.subset_to_merge'

    command = f'{path_to_rscript} {fix_strand_script} ' + \
              f'--prefix-own {prefix_own} ' + \
              f'--prefix-ref {prefix_ref} ' + \
              f'--prefix-out {prefix_own_subset} '
    run_shell_command(command)

    variant_list_fp = f'{prefix_own_subset}.variant_ids.list'
    fixed_bim_fp = f'{prefix_own_subset}.fixed.bim'

    check_input(variant_list_fp)
    check_input(fixed_bim_fp)

    # Subset the dataset
    command = f'{path_to_plink} ' + \
              f'--bfile {prefix_own} ' + \
              f'--extract {variant_list_fp} ' + \
              f'--make-bed --out {prefix_own_subset}'
    run_shell_command(command)

    check_input(f'{prefix_own_subset}.bed')
    check_input(f'{prefix_own_subset}.bim')
    check_input(f'{prefix_own_subset}.fam')

    subset_bim_fp = f'{prefix_own_subset}.bim'

    # Overwrite the BIM
    command = f'/bin/mv --verbose {fixed_bim_fp} {prefix_own_subset}.bim'
    run_shell_command(command)

    check_input(subset_bim_fp)

    # Merge the datasets
    command = f'{path_to_plink} ' + \
              f'--bfile {prefix_own_subset} ' + \
              f'--bmerge {prefix_ref} ' + \
              f'--extract {variant_list_fp} ' + \
              f'--make-bed --out {prefix_out}'
    run_shell_command(command)
