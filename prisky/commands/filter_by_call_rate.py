import click

from prisky.cli import cli
from prisky.logger import set_logfile, log_command_name_and_args
from prisky.helpers import run_shell_command, abs_label, check_input, check_output
from .command_options import label_option, path_to_plink_option


@cli.command()
@label_option
@click.option('--geno', default=0.1)
@click.option('--mind', default=0.1)
@path_to_plink_option
def filter_by_call_rate(label, geno, mind, path_to_plink):
    '''
    Run PLINK's --geno and --mind to filter the dataset individuals and
    varianta by call rate.
    '''
    dataset_label = abs_label(label)
    out_label = dataset_label + '.rate-filtered'
    set_logfile(out_label + '.log')
    log_command_name_and_args()

    check_input(dataset_label + '.bed')
    check_input(dataset_label + '.bim')
    check_input(dataset_label + '.fam')

    if not check_output(out_label + '.bed'):
        run_shell_command(
            f'{path_to_plink} ' +
            f'--bfile {dataset_label} ' +
            f'--mind {mind} ' +
            f'--geno {geno} ' +
            f'--make-bed --out {out_label}'
        )

    # We will also produce a new TSV of variants keeping only the set of
    # independent variants, for later analyses.

    variants_tsv = dataset_label + '.tsv'
    variants_file = out_label + '.variants_list'
    new_tsv = out_label + '.tsv'

    if not check_output(new_tsv):
        # Extract the variants from the new dataset
        run_shell_command(f"awk '{{ print $2 }}' {out_label}.bim > {variants_file}")
        # First extract the header of the TSV:
        run_shell_command(f'head -n1 {variants_tsv} > {new_tsv}')
        # Then append the matching variants:
        run_shell_command(f'grep -wf {variants_file} {variants_tsv} >> {new_tsv}')
