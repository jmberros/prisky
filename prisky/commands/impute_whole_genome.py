from pathlib import Path

from prisky.cli import cli
from prisky.logger import set_logfile, log_command_name_and_args
from prisky.helpers import run_shell_command, find_chromosome_datasets, \
    Timer, chrom_chunks, check_output, write_commands_to_file
from .command_options import label_option, path_to_impute2_option, \
    genetic_maps_dir_option, reference_dir_option, threads_option, \
    only_chrom_option, extra_args_option, path_to_parallel_option, \
    out_suffix_option


@cli.command()
@label_option
@path_to_impute2_option
@path_to_parallel_option
@genetic_maps_dir_option
@reference_dir_option
@threads_option
@only_chrom_option
@out_suffix_option
@extra_args_option
def impute_whole_genome(label, path_to_impute2, path_to_parallel,
                        genetic_maps_dir, reference_dir, threads,
                        out_suffix, extra_args="", only_chrom=None):
    '''
    Impute the whole genome for a dataset of {haps,sample} files from shapeIT.

     - *extra_args* are passed to impute2
     - *threads* is used for parallel --jobs
     - *out_suffix* is added to the output dir and commands list file

    '''
    logfile = f'{label}.impute_whole_genome.log'
    if only_chrom:
        logfile = logfile.replace('.log', f'.chr{only_chrom}.log')
    set_logfile(logfile)
    log_command_name_and_args()

    map_template = str(genetic_maps_dir/'genetic_map_chrN_combined_b37.txt')
    hap_template = str(reference_dir/'1000GP_Phase3_chrN.hap.gz')
    legend_template = str(reference_dir/'1000GP_Phase3_chrN.legend.gz')

    chrom_datasets = find_chromosome_datasets(label, exit_if_zero=True,
                                              extension="haps",
                                              only_chrom=only_chrom)

    prefix = label.name
    impute_results_dir = Path(f'{label}_impute2{out_suffix}')
    impute_results_dir.mkdir(exist_ok=True)

    commands = []

    for chrom_number, chrom_prefix in chrom_datasets:
        chrom_map = map_template.replace('N', chrom_number)
        chrom_reference_hap = hap_template.replace('N', chrom_number)
        chrom_reference_legend = legend_template.replace('N', chrom_number)

        chunks = chrom_chunks(chrom_number, step=5e6)
        for i, (chunk_start, chunk_stop) in enumerate(chunks):
            chunk_number = str(i + 1).zfill(2) # 01 instead of 1
            out_prefix = (f'{prefix}.chr{chrom_number}.chunk{chunk_number}.'
                          f'{chunk_start}-{chunk_stop}')

            if check_output(impute_results_dir/out_prefix):
                continue

            command = (
                f'{path_to_impute2} ' +
                f'-m {chrom_map} ' +
                f'-known_haps_g {chrom_prefix}.haps ' +
                f'-h {chrom_reference_hap} ' +
                f'-l {chrom_reference_legend} ' +
                '-Ne 20000 ' + # FIXME: do not hardcode this parameter
                f'-int {chunk_start} {chunk_stop} ' +
                f'-o {impute_results_dir}/{out_prefix}.gen ' +
                f'-i {impute_results_dir}/{out_prefix}.impute2_info ' +
                f'-r {impute_results_dir}/{out_prefix}.impute2_summary ' +
                f'-w {impute_results_dir}/{out_prefix}.impute2_warnings ' +
                extra_args
            )
            commands.append(command)

    commands_file = f'{label}.impute2{out_suffix}_commands.list'
    write_commands_to_file(commands, commands_file)

    timer = Timer()

    parallel_command = (
        f'{path_to_parallel} --jobs {threads} --verbose -- < {commands_file}'
    )
    run_shell_command(parallel_command)

    timer.stop_and_log()
