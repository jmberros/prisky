from prisky.cli import cli
from prisky.logger import logger, set_logfile, log_command_name_and_args
from prisky.helpers import quit_if_output_ready, abs_label, run_shell_command, \
    check_input
from .command_options import label_option


@cli.command()
@label_option
def make_variants_list(label):
    '''
    Produce a {label}.variant_ids.list file from a {label}.tsv file of variants
    (the TSV must have a column named 'variant_id').
    '''
    variants_tsv = abs_label(label) + '.tsv'
    out_path = abs_label(label) + '.variant_ids.list'
    set_logfile(f'{out_path}.log')
    log_command_name_and_args()
    check_input(variants_tsv)
    quit_if_output_ready(out_path)

    logger.info(f'Extract the IDs from: {variants_tsv}')
    run_shell_command(
        f"awk '{{ print $1 }}' {variants_tsv} | tail -n +2 > {out_path}"
    )
    logger.info(f'Written the list of variant IDs to: {out_path}')
