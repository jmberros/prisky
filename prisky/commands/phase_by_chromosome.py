from math import ceil

from prisky.cli import cli
from prisky.helpers import run_shell_command, find_chromosome_datasets, \
    check_input, check_output, Timer, write_commands_to_file
from prisky.logger import set_logfile, log_command_name_and_args
from .command_options import label_option, path_to_shapeit_option, \
    genetic_maps_dir_option, reference_dir_option, threads_option, \
    only_chrom_option, extra_args_option, path_to_parallel_option

@cli.command()
@label_option
@path_to_shapeit_option
@path_to_parallel_option
@genetic_maps_dir_option
@reference_dir_option
@threads_option
@only_chrom_option
@extra_args_option
def phase_by_chromosome(label, path_to_shapeit, path_to_parallel,
                        genetic_maps_dir, reference_dir,
                        threads, extra_args="", only_chrom=None):
    '''
    Phase chromosome datasets using shapeIT. Optionally do it for a single
    chromosone.
    '''
    logfile = f'{label}.phase_by_chromosome.log'
    if only_chrom:
        logfile = logfile.replace('.log', f'.chr{only_chrom}.log')
    set_logfile(logfile)
    log_command_name_and_args()

    map_template = str(genetic_maps_dir/'genetic_map_chrN_combined_b37.txt')
    hap_template = str(reference_dir/'1000GP_Phase3_chrN.hap.gz')
    legend_template = str(reference_dir/'1000GP_Phase3_chrN.legend.gz')
    ref_sample_file = reference_dir/'1000GP_Phase3.sample'

    chrom_datasets = find_chromosome_datasets(label, exit_if_zero=True,
                                              only_chrom=only_chrom)

    # TODO: This logic can be improved. We have `len(commands)` shapeIT commands
    # to run in `n_jobs` parallel jobs and we have to assign a number of
    # `threads_per_job` to each from a number of `total_threads`:
    total_threads = threads
    n_jobs = min(threads, len(chrom_datasets))
    threads_per_job = total_threads // n_jobs

    timer = Timer()

    commands = []
    for chrom_number, chrom_dataset_prefix in chrom_datasets:
        chrom_map = map_template.replace('N', chrom_number)
        chrom_reference_hap = hap_template.replace('N', chrom_number)
        chrom_reference_legend = legend_template.replace('N', chrom_number)
        chrom_alignments_file = \
            f'{chrom_dataset_prefix}.alignments.snp.strand.exclude'

        if check_output(f'{chrom_dataset_prefix}.haps'):
            continue

        check_input(chrom_alignments_file)
        command = (
            f'{path_to_shapeit} ' +
            f'--input-bed {chrom_dataset_prefix} ' +
            f'--input-map {chrom_map} ' +
            f'--input-ref {chrom_reference_hap} {chrom_reference_legend} {ref_sample_file} ' +
            f'--exclude-snp {chrom_alignments_file} ' +
            f'--output-max {chrom_dataset_prefix} ' +
            f'--output-log {chrom_dataset_prefix}.shapeit.log ' +
            f'--thread {threads_per_job} ' +
            extra_args
        )
        commands.append(command)

    commands_file = f'{label}.shapeIT_commands.list'
    write_commands_to_file(commands, commands_file)

    command = f'{path_to_parallel} --jobs {n_jobs} --verbose -- < {commands_file}'
    run_shell_command(command)

    # run_shell_command()
    timer.stop_and_log(f'Done! {len(chrom_datasets)} chromosomes took')


def primes(n):
    """
    Taken from: https://stackoverflow.com/a/16996439/1664522
    """
    primfac = []
    d = 2
    while d*d <= n:
        while (n % d) == 0:
            primfac.append(d)  # supposing you want multiple factors repeated
            n //= d
        d += 1
    if n > 1:
       primfac.append(n)
    return primfac
