import shutil
import sys

import click

from prisky.cli import cli
from prisky.logger import logger, log_command_name_and_args
from .command_options import (
    label_option,
    assembly_option,
    max_threads_option,
    path_to_tabix_option,
    path_to_java_option,
    path_to_bgzip_option,
    path_to_gatk3_option,
    path_to_plink_option,
    path_to_rscript_option,
    path_to_bcftools_option,
    path_to_reference_fasta_option,
)
from prisky.commands.ld_clumping_by_pvalue import ld_clumping_options
from prisky.commands import (
    prepare_UKBB_tsv,
    make_variants_list,
    make_score_files,
    make_regions_file,
    download_1KG_genotypes,
    rename_1KG_samples,
    make_plink_dataset,
    clump_by_pvalue,
    filter_by_call_rate,
    compute_genetic_score,
    plot_scores,
)
from prisky.helpers.common import (
    InputMissingException,
    EmptyInputException,
)
from prisky.commands.prepare_UKBB_tsv import (
    NoAssocFileException,
    ManyAssocFilesException,
)

@cli.command()
@label_option
@assembly_option
@ld_clumping_options
@max_threads_option
@path_to_tabix_option
@path_to_java_option
@path_to_bgzip_option
@path_to_gatk3_option
@path_to_plink_option
@path_to_rscript_option
@path_to_bcftools_option
@path_to_reference_fasta_option
@click.pass_context
def pipeline(ctx, **args):
    log_command_name_and_args()

    label = args['label']

    context_invoke(ctx,
                   prepare_UKBB_tsv,
                   label=label)

    context_invoke(ctx,
                   make_variants_list,
                   label=label)

    context_invoke(ctx,
                   make_regions_file,
                   label=label,
                   assembly=args['assembly'])

    context_invoke(ctx,
                   download_1KG_genotypes,
                   label=label,
                   assembly=args['assembly'],
                   path_to_java=args['path_to_java'],
                   path_to_bgzip=args['path_to_bgzip'],
                   path_to_tabix=args['path_to_tabix'],
                   path_to_gatk3=args['path_to_gatk3'],
                   path_to_bcftools=args['path_to_bcftools'],
                   path_to_reference_fasta=args['path_to_reference_fasta'])

    context_invoke(ctx,
                   rename_1KG_samples,
                   label=label,
                   path_to_bcftools=args['path_to_bcftools'])

    #---
    # Temporary step until the merging/preparation of the VCF is done
    vcf_1KG = '{label}.1KG.samples_renamed.vcf.gz'.format(**args)
    vcf_for_next_step = '{label}.vcf.gz'.format(**args)
    logger.info(f'Copy {vcf_1KG} -> {vcf_for_next_step}')
    shutil.copy2(vcf_1KG, vcf_for_next_step)
    #---

    context_invoke(ctx,
                   make_plink_dataset,
                   label=label,
                   path_to_plink=args['path_to_plink'])

    context_invoke(ctx,
                   clump_by_pvalue,
                   label=label,
                   path_to_plink=args['path_to_plink'])

    label = label + '.ld-pruned'
    ctx.params['label'] = label # Needed for the correct logging of params

    context_invoke(ctx,
                   filter_by_call_rate,
                   label=label,
                   path_to_plink=args['path_to_plink'])

    label = label + '.rate-filtered'
    ctx.params['label'] = label # Needed for the correct logging of params

    context_invoke(ctx,
                   make_score_files,
                   label=label)

    context_invoke(ctx,
                   compute_genetic_score,
                   label=label,
                   path_to_plink=args['path_to_plink'])

    context_invoke(ctx,
                   plot_scores,
                   label=label,
                   path_to_rscript=args['path_to_rscript'])


def context_invoke(ctx, func, **kwargs):
    try:
        ctx.invoke(func, **kwargs)
    except SystemExit:
        # Prevent the sys.exit called by helper method `quit_if_output_ready`
        pass
    except KeyboardInterrupt:
        logger.warning('User interrupt, existing.')
        sys.exit()
    except (InputMissingException,
            EmptyInputException,
            NoAssocFileException,
            ManyAssocFilesException):
        sys.exit()
