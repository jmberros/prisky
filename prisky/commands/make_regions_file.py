import prisky.helpers
from prisky.helpers import quit_if_output_ready, abs_label, check_input
from prisky.cli import cli
from prisky.logger import logger, set_logfile, log_command_name_and_args
from .command_options import label_option, assembly_option


@cli.command()
@label_option
@assembly_option
def make_regions_file(label, assembly):
    '''
    Produce a {label}.{assembly}.bed file from a {label}.variant_ids.list
    list of rs IDs. Optionally choose an assembly different from GRCh37.
    '''
    variants_list = abs_label(label) + '.variant_ids.list'
    out_path = abs_label(label) + f'.{assembly}.regions.bed'

    set_logfile(f'{out_path}.log')
    log_command_name_and_args()
    check_input(variants_list)
    quit_if_output_ready(out_path)

    with open(variants_list) as f:
        rs_ids = [l.strip() for l in f]

    logger.info(f'Found {len(rs_ids)} rs IDs in {variants_list}')

    prisky.helpers.make_bed_from_rs_ids(rs_ids=rs_ids,
                                        out_path=out_path,
                                        assembly=assembly)
