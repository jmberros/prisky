from prisky.cli import cli
from prisky.helpers import run_shell_command, find_chromosome_datasets, \
    check_output, Timer
from prisky.logger import set_logfile, log_command_name_and_args, logger
from .command_options import label_option, path_to_shapeit_option, \
    only_chrom_option

@cli.command()
@label_option
@path_to_shapeit_option
@only_chrom_option
def convert_haps_to_VCF(label, path_to_shapeit, only_chrom=None):
    '''
    Convert the chromosome files from shapeIT phasing, {haps,sample}, to VCF.
    '''
    logfile = f'{label}.convert_haps_to_VCF.log'
    if only_chrom:
        logfile = logfile.replace('.log', f'.chr{only_chrom}.log')
    set_logfile(logfile)
    log_command_name_and_args()

    chrom_datasets = find_chromosome_datasets(label,
                                              extension="haps",
                                              exit_if_zero=True,
                                              only_chrom=only_chrom)

    timer = Timer()

    for chrom_number, chrom_dataset_prefix in chrom_datasets:
        chrom_vcf = f'{chrom_dataset_prefix}.phased.vcf'

        if check_output(chrom_vcf):
            continue

        command = (
            f'{path_to_shapeit} ' +
            f'-convert ' +
            f'--input-haps {chrom_dataset_prefix} ' +
            f'--output-vcf {chrom_vcf}'
        )
        run_shell_command(command)

    logger.info(f'Converted {len(chrom_datasets)} {{haps,sample}} sets to VCF.')
    timer.stop_and_log(f'Done! {len(chrom_datasets)} chromosomes took')
