from pathlib import Path

from prisky.cli import cli
from prisky.helpers import run_shell_command, Timer
from prisky.logger import set_logfile, log_command_name_and_args
from .command_options import label_option, path_to_parallel_option, \
    threads_option, out_suffix_option, path_to_qctool_option


@cli.command()
@label_option
@path_to_parallel_option
@path_to_qctool_option
@threads_option
@out_suffix_option
def merge_impute2_chunk_files(label, path_to_parallel, path_to_qctool, threads,
                              out_suffix):
    '''
    Merge chunk files from impute2 output: .gen and .impute2_info.
    The result is one .gen and one .impute2_info.
    '''
    logfile = f'{label}.merge_impute2_chunk_files.log'
    set_logfile(logfile)
    log_command_name_and_args()

    # out_suffix, if present, is preceded by an underscore for the impute dir.
    # I want to use it as a suffix for the dataset label now.
    # For instance:
    #   impute dir: Pheno-1_impute2_some_suffix/
    #   new label: Pheno-1.some_suffix
    out_label = str(label) + out_suffix.replace('_', '.', 1)

    prefix = label.name
    impute_results_dir = Path(f'{label}_impute2{out_suffix}')

    timer = Timer()

    # Write the merged .info file header:

    input_glob = f'{impute_results_dir}/{prefix}.chr{{}}.chunk*.impute2_info'
    head_command = f'head --silent -n1 {input_glob}'
    output_file = f'{out_label}.impute2_info'

    # Merge all .impute2_info files, this means all chromosome and all chunks,
    # in the correct chrom-chunk order, with only ONE header:

    # Write .impute2_info header:
    command = (
        f"seq 1 22 | {path_to_parallel} '{head_command}' | uniq | tee {output_file}"
        # This prints the header of each .impute2_info file, and then merges
        # the unique cases. I'm assuming all .impute2_info files will have the
        # EXACT SAME header, so this will print only one header line to the
        # output file.
    )
    run_shell_command(command)

    # Merge all .impute2_info non-header rows in a single file:
    command = (
        'seq 1 22 | '+
        f'{path_to_parallel} --verbose --keep-order ' +
        f"'tail --silent -n+2 {input_glob} | sed s/^---/{{}}/' " + # parallel command
        f'>> {output_file}'
    )
    run_shell_command(command)

    # Merge .gen chunk files to a single .gen file PER CHROMOSOME:
    # (merging all .gen files across chromosomes would create a humongous file)

    input_glob = f'{impute_results_dir}/{prefix}.chr{{}}.chunk*.gen'
    output_chrom_file = f'{impute_results_dir}/{prefix}.chr{{}}.gen'
    command = (
        'seq 1 22 | ' +
        f'{path_to_parallel} --verbose --jobs {threads} ' +
        f"'cat {input_glob} > {output_chrom_file}'"
    )
    run_shell_command(command)

    # Merge the .gen chromosome files with qctool into a single .bgen.
    # This takes some time:

    command = (
        f'{path_to_qctool} -g {impute_results_dir}/{prefix}.chr#.gen ' +
        f'-og {out_label}.bgen'
    )
    run_shell_command(command)

    timer.stop_and_log()
