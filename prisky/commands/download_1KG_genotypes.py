from prisky.cli import cli
from prisky.logger import set_logfile, log_command_name_and_args
from prisky.helpers import quit_if_output_ready, run_shell_command, abs_label,\
    check_input
from .command_options import label_option, assembly_option
from .command_options import (
    path_to_java_option,
    path_to_bgzip_option,
    path_to_tabix_option,
    path_to_gatk3_option,
    path_to_bcftools_option,
    path_to_reference_fasta_option,
)

@cli.command()
@label_option
@assembly_option
@path_to_java_option
@path_to_bgzip_option
@path_to_tabix_option
@path_to_gatk3_option
@path_to_bcftools_option
@path_to_reference_fasta_option
def download_1KG_genotypes(label, assembly, **kwargs):
    '''
    Download genotypes from 1KG at the requested coordinates.
    '''
    bed = abs_label(label) + f'.{assembly}.regions.bed'
    check_input(bed)
    outlabel = abs_label(label) + f'.1KG'
    expected_out_path = f'{outlabel}.vcf.gz'
    set_logfile(f'{expected_out_path}.log')
    log_command_name_and_args()

    quit_if_output_ready(expected_out_path)

    command = (
        'bed_to_tabix '
        f'--in {bed} ' +
        f'--outlabel {outlabel} ' +
        '--remove-SVs ' +
        '--threads 24 ' + # should this be an option?
        f'--path-to-tabix {kwargs["path_to_tabix"]} ' +
        f'--path-to-bcftools {kwargs["path_to_bcftools"]} ' +
        f'--path-to-bgzip {kwargs["path_to_bgzip"]} ' +
        f'--path-to-java {kwargs["path_to_java"]} ' +
        f'--path-to-gatk3 {kwargs["path_to_gatk3"]} ' +
        f'--path-to-reference-fasta {kwargs["path_to_reference_fasta"]}'
    )
    run_shell_command(command)
