from prisky.cli import cli
from prisky.logger import logger, set_logfile, log_command_name_and_args
from prisky.helpers import find_score_columns, abs_label, read_variants, \
    check_output, check_input
from .command_options import label_option


@cli.command()
@label_option
def make_score_files(label):
    '''
    Make a scores file {label}.{score_tag}.scores.tsv from the {label}.tsv, for
    later use with PLINK. (One output file per `{score_tag}_score` column in
    the TSV).

    Also make a scores quantiles file {label}.{score_tag}.score_quantiles.tsv
    for later use by PLINK's --clump.
    '''
    variants_tsv = abs_label(label) + '.tsv'
    set_logfile(abs_label(label) + '.make_score_files.log')
    log_command_name_and_args()
    check_input(variants_tsv)

    variants = read_variants(variants_tsv)
    score_columns = find_score_columns(variants)

    for score_column in score_columns:
        score_tag = score_column.replace('_score', '')

        # --- Make the scores file ---
        out_path = abs_label(label) + f'.{score_tag}.scores.tsv'

        if not check_output(out_path):
            cols = ['variant_id', 'effect_allele', score_column]
            variants[cols].to_csv(out_path, sep='\t', header=False, index=False)
            logger.info(f'Written a scores file to: {out_path}')

        # --- Make the score quantiles file ---
        out_path = abs_label(label) + f'.{score_tag}.score_quantiles.tsv'
        quantile_column = f'{score_column}_quantile'

        if not check_output(out_path):
            ranked_variants = (variants
                               .sort_values(by=score_column, ascending=False)
                               .reset_index()
                               .rename(columns={'index': 'original_order'})
                               .reset_index()
                               .rename(columns={'index': 'rank'})
                               .sort_values(by='original_order'))
            ranked_variants['rank'] += 1
            ranked_variants[quantile_column] = (
                (ranked_variants['rank'] / len(ranked_variants))
                .map(lambda value: round(value, 5))
            )

            cols = ['variant_id', score_column, quantile_column]
            ranked_variants[cols].to_csv(out_path, sep='\t', header=True, index=False)
            logger.info(f'Written a score quantiles file to: {out_path}')
