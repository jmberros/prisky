from os import environ

from prisky.helpers import CPU_COUNT, make_Path, \
    add_leading_underscore_if_present

import click


def label_option(func):
    return click.option('--label', required=True, callback=make_Path)(func)

def assembly_option(func):
    return click.option('--assembly', required=True, default='GRCh37')(func)

def extra_args_option(func):
    return click.option('--extra-args', required=False, default='')(func)

def max_threads_option(func):
    return click.option('--max-threads', default=CPU_COUNT)(func)

def threads_option(func):
    return click.option('--threads', default=CPU_COUNT)(func)

def genetic_maps_dir_option(func):
    return click.option('--genetic-maps-dir',
                        callback=make_Path, required=True)(func)

def reference_dir_option(func):
    return click.option('--reference-dir',
                        callback=make_Path, required=True)(func)

def only_chrom_option(func):
    return click.option('--only-chrom', type=str, required=False)(func)

def out_suffix_option(func):
    return click.option('--out-suffix', required=False, default='',
                        callback=add_leading_underscore_if_present)(func)

def path_to_resource_option(func, resource_name):
    resource_name_upped = resource_name.upper().replace('-', '_')
    return click.option(f'--path-to-{resource_name}',
                        default=environ.get(f'PATH_TO_{resource_name_upped}'),
                        type=click.Path(exists=True),
                        required=True)(func)

def path_to_plink_option(func):
    return path_to_resource_option(func, 'plink')

def path_to_tabix_option(func):
    return path_to_resource_option(func, 'tabix')

def path_to_bcftools_option(func):
    return path_to_resource_option(func, 'bcftools')

def path_to_bgzip_option(func):
    return path_to_resource_option(func, 'bgzip')

def path_to_java_option(func):
    return path_to_resource_option(func, 'java')

def path_to_gatk3_option(func):
    return path_to_resource_option(func, 'gatk3')

def path_to_reference_fasta_option(func):
    return path_to_resource_option(func, 'reference-fasta')

def path_to_rscript_option(func):
    return path_to_resource_option(func, 'rscript')

def path_to_shapeit_option(func):
    return path_to_resource_option(func, 'shapeit')

def path_to_impute2_option(func):
    return path_to_resource_option(func, 'impute2')

def path_to_parallel_option(func):
    return path_to_resource_option(func, 'parallel')

def path_to_qctool_option(func):
    return path_to_resource_option(func, 'qctool')
