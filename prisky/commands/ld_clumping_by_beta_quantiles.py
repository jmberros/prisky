from os.path import isfile

import pandas as pd

from prisky.cli import cli
from prisky.logger import logger, set_logfile
from prisky.helpers import run_shell_command, find_score_columns, abs_label
from .command_options import label_option, path_to_plink_option


@cli.command()
@label_option
@path_to_plink_option
def clump_by_beta_quantiles(label, path_to_plink):
    '''
    Clump the variants of a PLINK dataset based on linkage disequilibrium.
    Produces a .beta-clumped file for each of the scores in the dataset.

    The logic of clumping/pruning via PLINK is the following. We want to clump
    (group) the variants that are in linkage disequilibrium. For each group, we
    want to keep the variant that has the "most powerful" effect, i.e. the
    effect with the greatest absolute value. So we use PLINK's --clump option,
    but instead of priorizing by p-value, we do the priorizing by effect size.
    Since we do not want to leave any variant out because of their effect size,
    we use --clump-p1=1 and --clump-p2=1 (1 is the top value). Since clumping
    by p-value means priorizing the *lowest* p-value, but we want to priorize
    the *highest* effect size, we use the quantile of effect size, which is a
    decreasing function of the effect size.
    '''
    dataset_tag = abs_label(label)
    variants_tsv = dataset_tag + '.tsv'
    score_columns = find_score_columns(variants_tsv)
    score_tags = [score_column.split('_')[0] for score_column in score_columns]

    for score_tag in score_tags:
        make_clumped_file(dataset_tag, score_tag, path_to_plink)

    # For now, I will just *notify* the user of the existence of some variants
    # that need to be pruned before continuing.
    # The fully functional command should produce a new PLINK dataset with
    # the selected variants, that is, removing the variants in LD with another
    # variant that has a more powerful effect on the phenotype.

    for score_tag in score_tags:
        set_logfile(f'{dataset_tag}.{score_tag}.check.log')

        try:
            check_if_variants_need_to_be_pruned(dataset_tag, score_tag)
            logger.info(f'No clumped variants for the "{score_tag}" score.')
        except VariantsNeedToBePrunedException:
            logger.warning(f'Some variants are clumped for the "{score_tag}" score.')

    # There should be one filtered dataset per score, I guess, although they
    # are probably going to be the same.


def make_clumped_file(dataset_tag, score_tag, path_to_plink):
    clumped_file = f'{dataset_tag}.{score_tag}.beta-clumped'
    set_logfile(f'{clumped_file}.log')

    if not isfile(clumped_file):
        command = (
            f'{path_to_plink} --bfile {dataset_tag} ' +
            f'--clump {dataset_tag}.{score_tag}.score_quantiles.tsv ' +
            '--clump-p1 1 --clump-p2 1 '  + # Include variants in every quantile
            '--clump-snp-field variant_id ' +
            f'--clump-field {score_tag}_score_quantile ' +
            f'--out {dataset_tag}.{score_tag}'
        )
        run_shell_command(command)

    logger.info(f'Output ready: {clumped_file}')


def check_if_variants_need_to_be_pruned(dataset_tag, score_tag):
    clumped_file = f'{dataset_tag}.{score_tag}.beta-clumped'
    clumps = pd.read_csv(clumped_file, sep=r'\s+')
    if any(clumps['SP2'] != 'NONE'):
        raise VariantsNeedToBePrunedException


class VariantsNeedToBePrunedException(Exception):
    pass
