import subprocess

from termcolor import colored

from prisky.logger import logger


def run_shell_command(command, silence_stdout=False):
    """Run a *command* (string) in the shell. Return the output."""
    colored_command = colored(command, 'magenta')
    logger.info(f'Running:\n\n  {colored_command} \n')

    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT, shell=True)

    output_lines = []

    if not silence_stdout:
        logger.info('Output: \n')

    while True:
        output = process.stdout.readline()
        output = output.decode('utf-8')
        command_finished = (output == '' and process.poll() is not None)
        if command_finished:
            break
        if not silence_stdout:
            logger.info(colored('  ' + output.rstrip("\n"), 'blue'))
        output_lines.append(output)

    if not silence_stdout:
        logger.info('')

    return output_lines
