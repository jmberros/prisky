import re
from pathlib import Path
from os.path import join, expanduser, dirname, getsize, isfile
import sys
import multiprocessing
from glob import glob

import pandas as pd
from termcolor import colored

from prisky.logger import logger


package_root = Path(dirname(dirname(dirname(__file__))))
R_dir = package_root/'priskyr'/'R'
CPU_COUNT = multiprocessing.cpu_count()


# Option callbacks:


def add_leading_underscore_if_present(ctx, param, value):
    return f'_{value}' if (value and not value.startswith('_')) else value


def make_Path(ctx, param, value):
    return Path(value)


# Other helpers:


def write_commands_to_file(commands_list, commands_file):
    '''
    Write a `commands_list` to a `commands_file`. Overwrites it present.
    '''
    with open(commands_file, 'w') as f:
        for command in commands_list:
            f.write(command + '\n')

    logger.info(f'Written {len(commands_list)} commands to: {commands_file}')
    return commands_file


def chrom_dir_for_label(labelpath):
    '''
    Given a Path object that points to a PLINK prefix, return a Path object
    with the directory where chromosome files should be put or found.
    '''
    return labelpath.parent/f'{labelpath.name}_chrom_files'


def find_chromosome_datasets(label, exit_if_zero=False, only_chrom=None,
                             extension="bed"):
    '''
    Given a Path object *label* associated to a PLINK dataset, find all PLINK
    datasets of single chromosomes for that dataset assuming they live in a
    subdirectory called {label}_chrom_files. Returns a list of tuples
    (chrom_number, chrom_dataset_prefix).

    If *only_chrom* is provided, just search for a particular chromosome
    dataset.

    If *exit_if_zero* is true, it will sys.exit logging an error.
    '''
    chromosomes_dir = label.parent/f'{label.name}_chrom_files'
    chrom_dataset_pattern = str(chromosomes_dir/f'{label.name}.chr*.{extension}')
    prefixes = sorted(fp.replace(f'.{extension}', '')
                      for fp in glob(chrom_dataset_pattern))
    chrom_numbers = [re.search(r'\.chr(.+)$', prefix).group(1)
                     for prefix in prefixes]
    chrom_datasets = [(chrom, prefix)
                      for chrom, prefix in zip(chrom_numbers, prefixes)]

    if only_chrom is not None:
        chrom_datasets = [(chrom, prefix)
                          for chrom, prefix in zip(chrom_numbers, prefixes)
                          if chrom == str(only_chrom)]

    if not len(chrom_datasets) and exit_if_zero:
        logger.error(f"No chromosome Plink datasets in: {chromosomes_dir}/")
        sys.exit()

    return chrom_datasets


def abs_label(labelpath):
    '''
    Given a *labelpath* like '~/path/to/work/Pheno-1' return the absolute-path
    label: '/home/user/path/to/work/Pheno-1'. '''
    # return abspath(expanduser(labelpath))
    # NOTE: I stopped using absolute paths because they are too verbose in the
    # logs and I do not see the advantage anymore. I'm not 100% sure of this
    # decision, though, so I'm leaving the previous line available for an easy
    # rollback to the previous behavior.
    return expanduser(labelpath)


def check_input(path):
    '''
    Checks if an input file exists and raises if it does not.
    '''
    if not isfile(path):
        logger.error(f'Missing input file:\n\n {colored(path, "red")} \n')
        raise InputMissingException()
    if not getsize(path):
        logger.error(f'Input file seems empty:\n\n {colored(path, "red")} \n')
        raise EmptyInputException()
    return True

class InputMissingException(Exception): pass
class EmptyInputException(Exception): pass
class OutputMissingException(Exception): pass


def check_output(path, raise_error=False):
    '''
    Checks if a file exists and logs a message if it does exist.
    '''
    file_exists = isfile(path) and getsize(path) != 0
    if file_exists:
        logger.info(f'Output already exists:\n\n  {colored(path, "green")} \n')
    elif raise_error:
        logger.error(f'Missing expected output: \n\n {path} \n')
        raise OutputMissingException()

    return file_exists


def quit_if_output_ready(out_path):
    if check_output(out_path):
        sys.exit()


def get_1KG_samples():
    '''
    Get a pandas.DataFrame with the sample ids and populations of the 1000
    Genomes Project.
    '''
    filename = 'integrated_call_samples_v3.20130502.ALL.panel'
    filepath = join(dirname(dirname(__file__)), 'sources', filename)
    return pd.read_csv(filepath, sep=r'\s+')


def chrom_lengths():
    '''
    A dict of human chromosome lengths.
    '''
    filename = 'chromosome_lengths.csv'
    filepath = join(dirname(dirname(__file__)), 'sources', filename)
    df = pd.read_csv(filepath, names=["chrom", "length"])
    df["length"] = df["length"].astype(int)
    return df.set_index("chrom")["length"].to_dict()


def chrom_chunks(chrom_number, step):
    '''
    Returns a list of tuples (start, stop) that span the whole chromosome.
    Each start-stop interval has exactly *step* length - 1.
    '''
    chrom_length = chrom_lengths()[str(chrom_number)]

    # We're talking about basepairs steps, so floats make no sense here
    step = int(step)
    chrom_length = int(chrom_length)

    chrom_chunks = [(1, step)]
    while chrom_chunks[-1][1] < chrom_length:
        new_chunk_start = chrom_chunks[-1][1] + 1
        new_chunk_stop = new_chunk_start - 1 + step
        chrom_chunks.append((new_chunk_start, new_chunk_stop))

    return chrom_chunks


def read_variants(path):
    '''
    Read the variants TSV at *path*.
    '''
    return pd.read_csv(path, sep=r'\s+')
