import time

from humanfriendly import format_timespan

from prisky.logger import logger


class Timer:
    def __init__(self):
        self.start()

    def start(self):
        self.start_time = time.time()

        # Clear previous runs:
        self.stop_time = None
        self.elapsed_time = None
        self.elapsed_time_formatted = None

    def stop(self):
        self.stop_time = time.time()
        self.elapsed_time = self.stop_time - self.start_time
        self.elapsed_time_formatted = format_timespan(self.elapsed_time)

    def log(self, message):
        logger.info(f'{message} {self.elapsed_time_formatted}')

    def stop_and_log(self, message='Done! Took:'):
        self.stop()
        self.log(message)
