import re

import pandas as pd
from anotala.annotators import EnsemblAnnotator
from vcf_to_dataframe.helpers import make_chromosome_series_categorical

from prisky.logger import logger


def make_bed_from_rs_ids(rs_ids, out_path, assembly='GRCh37'):
    '''
    Make a BED file at *out_path* from a list of *rs_ids*.
    '''
    raise_if_any_id_is_not_rs_id(rs_ids)

    logger.info(f'Annotate positions for {len(rs_ids)} rs IDs')

    # Annotate
    EnsemblAnnotator.full_info = False
    ensembl = EnsemblAnnotator(cache='mysql')
    annotations = list(ensembl.annotate(rs_ids).values())
    df = pd.DataFrame.from_records(annotations)

    # Parse annotations for the BED
    df['start'] = df['mappings'].map(extract_start, assembly)
    df['end'] = df['mappings'].map(extract_end, assembly)
    df['chrom'] = df['mappings'].map(extract_chromosome)
    df['chrom'] = make_chromosome_series_categorical(df['chrom'])

    bed_cols = 'chrom|start|end|name'.split('|')
    bed = df[bed_cols].sort_values(by=['chrom', 'start'])
    bed = bed.dropna(how='any')

    logger.info(f'{len(bed)} variants with coordinates found')

    # Write the BED
    logger.info(f'Write BED to: {out_path}')
    bed.to_csv(out_path, sep='\t', header=False, index=False)


def raise_if_any_id_is_not_rs_id(ids):
    non_rsids = [id_ for id_ in ids if not re.search(r'^rs\d+$', id_)]
    if non_rsids:
        l = '\n'.join(non_rsids)
        raise Exception(f'{len(non_rsids)} IDs are not rs IDs:\n\n"{l}"')


def choose_mapping(mappings, assembly_name):
    mappings_ = [m for m in mappings if m['assembly_name'] == assembly_name]
    # Avoid weird contigs and alternative chromosome sections, etc:
    mappings_= [m for m in mappings if m['coord_system'] == 'chromosome']
    # This keeps only chromosomes like 1..22, X, Y:
    mappings_ = [m for m in mappings if len(m['seq_region_name']) <= 2]

    if not len(mappings_) == 1:
        logger.error(f'Many or too few mappings for this variant. ' +
                     f'Available mappings: {mappings}')
        import pprint
        pprint.pprint(mappings)
        # raise VariantMappingsException()
        return

    return mappings_[0]

# class VariantMappingsException(Exception): pass


def extract_mapping_key(mappings, assembly_name, key):
    mapping = choose_mapping(mappings, assembly_name)
    return mapping.get(key)


def extract_start(mappings, assembly_name='GRCh37'):
    return extract_mapping_key(mappings, assembly_name, 'start')


def extract_end(mappings, assembly_name='GRCh37'):
    return extract_mapping_key(mappings, assembly_name, 'end')


def extract_chromosome(mappings, assembly_name='GRCh37'):
    return extract_mapping_key(mappings, assembly_name, 'seq_region_name')
