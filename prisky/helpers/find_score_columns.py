from pathlib import Path

from termcolor import colored
import pandas as pd

from prisky.helpers import read_variants
from prisky.logger import logger


def find_score_columns(df_or_path):
    """
    Returns a list of columns that end in _score. If a path is provided, it
    will be assumed a TSV lives in that path.
    """
    if isinstance(df_or_path, pd.DataFrame):
        return _find_score_columns_in_df(df_or_path)
    elif isinstance(df_or_path, str) or isinstance(df_or_path, Path):
        df = read_variants(df_or_path)
        return _find_score_columns_in_df(df)
    else:
        raise ValueError('I need either a pandas DataFrame or a path to a TSV, '
                         f'but I received a {df_or_path.__class__.__name__}.')


def _find_score_columns_in_df(df):
    """
    Returns a list of columns that end in _score in the *df*.
    """
    score_columns = [col for col in df.columns if col.endswith('_score')]
    logger.info(f'Found {len(score_columns)} score fields: '+
                ', '.join(colored(col, 'green') for col in score_columns))
    return score_columns
