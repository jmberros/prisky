import os
import logging
from datetime import datetime

import click
from termcolor import colored


btt_logger = logging.getLogger('bed_to_tabix')
btt_logger.setLevel(logging.WARNING)

anotala_logger = logging.getLogger('anotala')
anotala_logger.setLevel(logging.WARNING)

logger = logging.getLogger(__name__)
# TODO: do not hardcode the logging level
logger.setLevel(logging.INFO)

stream_handler = logging.StreamHandler()

## Old formatter with no level-based colors:
# formatter = logging.Formatter("%(message)s")
# stream_handler.setFormatter(formatter)

## Formatter with level-based colors
from colorlog import ColoredFormatter
formatter = ColoredFormatter(
    "%(log_color)s%(message)s%(reset)s",
    log_colors={
        'DEBUG': 'cyan',
        'INFO': 'white',
        'WARNING': 'bold_yellow',
        'ERROR': 'bold_red',
        'CRITICAL': 'red,bg_white',
    }
)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


def set_logfile(path=None):
    '''
    Set the logfile to *path* and remove other sinks. If *path* is None,
    remove all logfiles. Also adds a timestamp to path before ".log".
    '''
    loggers = [btt_logger, anotala_logger, logger]

    for logger_ in loggers:
        logger_.handlers = []  # Remove previous file sinks!
        logger_.addHandler(stream_handler)

    if path:
        timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
        path = str(path).replace('.log', f'.{timestamp}.log')
        logger.info(f'Logging at: {path}')

        if os.path.isfile(path):
            os.remove(path)

        for logger_ in loggers:
            file_handler = logging.FileHandler(path)
            file_handler.setFormatter(formatter)
            logger_.addHandler(file_handler)


# FIXME: untested function! Change with caution.
def log_command_name_and_args():
    context = click.get_current_context()
    command_name = context.info_name
    command_pretty_name = colored(command_name.replace('-', ' ').title(),
                                  attrs=['bold', 'reverse'])
    message = 'Options received: \n'
    message += f'\n  * Invoked command: {command_pretty_name}'

    params = {**(context.params or {}),
              **((context.parent and context.parent.params) or {})}
    command_param_names = [param.name for param in context.command.params]
    params = {name: value for name, value in params.items()
              if name in command_param_names}
    if params:
        for param_name, param_value in params.items():
            message += f'\n  * {param_name} = '
            if param_name == 'label':
                message += colored(str(param_value), 'green')
            else:
                message += str(param_value)

    logger.info(message + '\n')
