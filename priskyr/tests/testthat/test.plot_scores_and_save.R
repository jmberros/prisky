context("Plot the scores per population from a profiles dataframe")

test_that("a path for the plot is generated", {
  result <- make_plot_path("path/to/Pheno-1.foo.bar.baz", "score-tag", "AFR", F)
  expect_equal(result, "path/to/Pheno-1.foo.bar.baz.score-tag.AFR.png")

  result <- make_plot_path("Pheno-1.foo.bar.baz", "score-tag", "AFR", T)
  expect_equal(result, "Pheno-1.foo.bar.baz.score-tag.ALL.png")
})

test_that("a title for the plot is generated", {
  result <- make_fig_title(dataset_name = "Pheno-1.foo.bar.ld-pruned",
                           score_tag = "basic")
  expect_equal(result, "Pheno-1 Foo Bar - Basic Score")
})

test_that("a plot of scores per population is drawn", {
  fake_profiles_df <- tribble(
    ~population, ~super_population, ~SCORESUM,

    "POP-1",     "SUP-1",           1,
    "POP-1",     "SUP-1",           2,
    "POP-1",     "SUP-1",           -1,

    "POP-2",     "SUP-1",           3,
    "POP-2",     "SUP-1",           0,
    "POP-2",     "SUP-1",           -3,

    "POP-3",     "SUP-2",           4,
    "POP-3",     "SUP-2",           -0.5,
    "POP-3",     "SUP-2",           -3
  )

  fig <- plot_scores_and_save(super_population = "SUP-1",
                              plot_all_super_populations = T,
                              dataset_name = "Pheno-1.foo.bar",
                              score_tag = "score-tag",
                              profiles_df = fake_profiles_df,
                              plot_path = NULL)

  # Plot labels
  expect_equal(fig$labels$title, "Pheno-1 Foo Bar - Score-Tag Score")
  expect_equal(fig$labels$x, "Score-Tag Score")
  expect_equal(fig$labels$y, "Population")
  expect_equal(fig$labels$fill, "Score-Tag Score")
  # Plot data
  expect_equal(nrow(fig$data), 9) # Because all super populations checked
  # Plot aesthetic mapping
  expect_equal(rlang::quo_text(fig$mapping$x), "SCORESUM")
  expect_equal(rlang::quo_text(fig$mapping$y), "population")
  expect_equal(rlang::quo_text(fig$mapping$fill), "..x..")

  test_plot_path <- file.path(tempdir(), "test-profiles-plot.png")

  fig <- plot_scores_and_save(super_population = "SUP-1",
                              plot_all_super_populations = F,
                              dataset_name = "Pheno-1.foo.bar",
                              score_tag = "score-tag",
                              profiles_df = fake_profiles_df,
                              plot_path = test_plot_path)

  expect_equal(nrow(fig$data), 6) # Because only SUP-1 individuals chosen
  expect_equal(rlang::quo_text(fig$mapping$fill), "..x..")
  expect_true(file.exists(test_plot_path))
})

test_that("the plot function wrapper is ok", {
  file.copy("files/Pheno-1.ld-pruned.basic.profile", tempdir())
  input <- list(profiles_file = file.path(tempdir(), "Pheno-1.ld-pruned.basic.profile"),
                super_population = "AMR",
                all_super_populations = T)
  result <- plot_scores_and_save_wrapper(input)
  expect_equal(class(result)[2], "ggplot")
})

