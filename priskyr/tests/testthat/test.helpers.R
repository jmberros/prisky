context("Helper functions")

test_that("profile files can be found given a root directory", {
  root_dir <- tempdir()
  dir_1 <- file.path(root_dir, "Pheno-1")
  dir_2 <- file.path(root_dir, "Pheno-2")
  dir.create(dir_1)
  dir.create(dir_2)
  file.create(file.path(dir_1, "Pheno-1.score-1.profile"))
  file.create(file.path(dir_1, "Pheno-1.score-2.profile"))
  file.create(file.path(dir_1, "Pheno-1.something-else.txt"))
  file.create(file.path(dir_2, "Pheno-2.score-1.profile"))

  # root_dir
  # |_ Pheno-1
  # |  |_ Pheno-1.score-1.profile
  # |  |_ Pheno-1.score-2.profile
  # |  |_ Pheno-1.something-else.txt
  # |_ Pheno-2
  #    |_ Pheno-2.score-1.profile

  result <- find_profile_paths_recursively(root_dir)

  expect_equal(length(result), 3)
})
