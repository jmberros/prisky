pytest_plugins = ["helpers_namespace"]

from pathlib import Path
from os.path import join, dirname
import subprocess
from unittest.mock import Mock
import shutil

import pytest
from click.testing import CliRunner


@pytest.helpers.register
def file(filename):
    """Return the full path to a test file under tests/files."""
    return join(dirname(__file__), "files", filename)


@pytest.fixture
def tmp_work_dir(tmp_path):
    """
    This fixture is meant to set a temporary work directory with a test TSV
    that will be the input for the analysis to be tested.
    """
    temporary_dir = tmp_path/'Test_Pheno'
    temporary_dir.mkdir()
    tsv = pytest.helpers.file('Test_Pheno.tsv')
    shutil.copy2(tsv, temporary_dir)
    return temporary_dir


@pytest.fixture
def tmp_label(tmp_work_dir):
    '''
    This fixture is meant to give a temporary label for testing with a real TSV
    available for the commands.
    '''
    return tmp_work_dir/'Test_Pheno'


@pytest.helpers.register
def touch(path):
    '''
    Create the file at *path* with some content in it so that `check_input`
    does not fail on it.
    '''
    Path(path).touch()
    Path(path).write_text('some content')
    return path


@pytest.fixture
def cli_runner():
    return CliRunner()


@pytest.fixture
def mock_Popen(monkeypatch):
    mock_process = Mock()
    mock_process.stdout.readline = Mock(return_value=b'')
    mock_process.poll = Mock(return_value=True)
    mock_Popen = Mock(return_value=mock_process)
    monkeypatch.setattr(subprocess, 'Popen', mock_Popen)
    return mock_Popen
