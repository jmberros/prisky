from os.path import isfile
import shutil

import pytest
import pandas as pd

from prisky.commands.prepare_UKBB_tsv import (
    prepare_UKBB_tsv,
    NoAssocFileException,
    ManyAssocFilesException,
    find_UKBB_assoc_tsv,
    remove_multiline_variants,
    keep_significant_variants,
    extract_effect_allele,
    remove_nonrsid_variants,
)


def test_remove_multiline_variants():
    df = pd.DataFrame({'rsid': ['rs1', 'rs2', 'rs1', 'rs3']})
    result = remove_multiline_variants(df)
    assert list(result['rsid']) == ['rs2', 'rs3']

def test_keep_significant_variants():
    df = pd.DataFrame({'pval': [0.1, 0.01, 0.001, 0.0001]})
    result = keep_significant_variants(df)
    assert list(result['pval']) == [0.001, 0.0001]

def test_extract_effect_allele():
    result = extract_effect_allele('Foo:C:T')
    assert result == 'T'

def test_remove_nonrsid_variants():
    df = pd.DataFrame({'rsid': ['rs1', 'rs2', 'non-rs']})
    result = remove_nonrsid_variants(df)
    assert list(result['rsid']) == ['rs1', 'rs2']

def test_find_UKBB_assoc_tsv(tmp_path):
    with pytest.raises(NoAssocFileException):
        find_UKBB_assoc_tsv(tmp_path/'Non-existent-label')

    (tmp_path/'UKBB_Pheno.1.assoc.tsv').touch()

    result = find_UKBB_assoc_tsv(tmp_path/'UKBB_Pheno')
    assert result == str(tmp_path/'UKBB_Pheno.1.assoc.tsv')

    # If more than one assoc file exists, raise an error
    (tmp_path/'UKBB_Pheno.2.assoc.tsv').touch()

    with pytest.raises(ManyAssocFilesException):
        find_UKBB_assoc_tsv(tmp_path/'UKBB_Pheno')


def test_prepare_UKBB_tsv(tmp_path, cli_runner):
    tsv = pytest.helpers.file('UKBB_Pheno.1234.assoc.tsv')
    shutil.copy2(tsv, tmp_path)
    tmp_label = tmp_path/'UKBB_Pheno'

    result = cli_runner.invoke(
        prepare_UKBB_tsv,
        ['--label', tmp_label],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    out_path = f'{tmp_label}.tsv'
    assert isfile(out_path)

    result_df = pd.read_table(out_path)
    assert list(result_df.columns) == \
        ['variant_id', 'effect_allele', 'basic_score', 'p_value']

    # Checks the non-significant, the triallelic, and the non-rsid variants
    # were removed:
    assert len(result_df) == 1
    assert result_df.loc[0].to_dict() == {
        'variant_id': 'rs1',
        'effect_allele': 'T',
        'basic_score': 0.01,
        'p_value': 0.0001,
    }
