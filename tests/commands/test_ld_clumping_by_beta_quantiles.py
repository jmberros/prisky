from unittest.mock import Mock

import pytest

import prisky
from prisky.commands.ld_clumping_by_beta_quantiles import (
    clump_by_beta_quantiles,
    check_if_variants_need_to_be_pruned,
    VariantsNeedToBePrunedException
)


def test_clump_by_beta_quantiles(tmp_label, cli_runner, mock_Popen, monkeypatch):
    mock_check_if_variants_need_to_be_pruned = Mock()
    monkeypatch.setattr(prisky.commands.ld_clumping_by_beta_quantiles,
                        'check_if_variants_need_to_be_pruned',
                        mock_check_if_variants_need_to_be_pruned)

    result = cli_runner.invoke(
        clump_by_beta_quantiles,
        ['--label', tmp_label,
         '--path-to-plink', pytest.helpers.file('fake_resources/plink')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 2

    command_1 = mock_Popen.call_args_list[0][0][0]
    assert f'plink --bfile {tmp_label}' in command_1
    assert f'--clump {tmp_label}.basic.score_quantiles.tsv' in command_1
    assert '--clump-p1 1 --clump-p2 1' in command_1
    assert '--clump-snp-field variant_id --clump-field basic_score_quantile'\
        in command_1
    assert f'--out {tmp_label}' in command_1

    command_2 = mock_Popen.call_args_list[1][0][0]
    assert f'plink --bfile {tmp_label}' in command_2
    assert f'--clump {tmp_label}.adjusted.score_quantiles.tsv' in command_2
    assert '--clump-p1 1 --clump-p2 1' in command_2
    assert '--clump-snp-field variant_id --clump-field adjusted_score_quantile'\
        in command_2
    assert f'--out {tmp_label}.adjusted' in command_2

    assert mock_check_if_variants_need_to_be_pruned.call_count == 2


def test_check_if_variants_need_to_be_pruned(tmp_label):
    fake_clumped_file = f'{tmp_label}.basic.beta-clumped'
    with open(fake_clumped_file, 'w') as f:
        f.write('SNP\tSP2\n')
        f.write('rs1\tNONE\n')
        f.write('rs2\tNONE\n')

    # Doesn't raise anything:
    check_if_variants_need_to_be_pruned(tmp_label, 'basic')

    with open(fake_clumped_file, 'w') as f:
        f.write('SNP\tSP2\n')
        f.write('rs1\tNONE\n')
        f.write('rs2\trs1(1)\n')
    with pytest.raises(VariantsNeedToBePrunedException):
        check_if_variants_need_to_be_pruned(tmp_label, 'basic')
