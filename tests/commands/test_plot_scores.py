import re

import pytest

from prisky.commands import plot_scores
from prisky.helpers import package_root


def test_plot_scores(tmp_label, cli_runner, mock_Popen):
    result = cli_runner.invoke(
        plot_scores,
        ['--label', tmp_label,
         '--path-to-rscript', pytest.helpers.file('fake_resources/Rscript')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    app_dir = package_root/'priskyr'
    assert mock_Popen.call_count == 1
    assert re.search(
        rf'.+/Rscript .+/prisky/priskyr/cli.R --app-dir {app_dir} --label {tmp_label} --port 7575 --host 0.0.0.0',
        mock_Popen.call_args[0][0],
    )
