import re

import pytest

from prisky.commands import make_plink_dataset


def test_make_plink_dataset(tmp_label, cli_runner, mock_Popen):
    in_vcf = f'{tmp_label}.vcf.gz'
    pytest.helpers.touch(in_vcf)

    result = cli_runner.invoke(
        make_plink_dataset,
        ['--label', tmp_label,
         '--path-to-plink', pytest.helpers.file('fake_resources/plink')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 1
    assert re.search(
        rf'.+/plink --vcf {in_vcf} --make-bed --out {tmp_label}',
        mock_Popen.call_args[0][0],
    )
