import re

import pytest

from prisky.commands import convert_haps_to_VCF


def test_convert_haps_to_VCF(tmp_label, cli_runner, mock_Popen):
    chrom_dir = tmp_label.parent/f'{tmp_label.name}_chrom_files'
    prefix = tmp_label.name

    chrom_1_label = chrom_dir/f'{prefix}.chr1.phased'
    chrom_19_label = chrom_dir/f'{prefix}.chr19.phased'

    # The command checks which chrom datasets are present in the chrom dir:
    chrom_dir.mkdir()
    pytest.helpers.touch(f'{chrom_1_label}.haps')
    pytest.helpers.touch(f'{chrom_1_label}.sample')
    pytest.helpers.touch(f'{chrom_19_label}.haps')
    pytest.helpers.touch(f'{chrom_19_label}.sample')

    result = cli_runner.invoke(
        convert_haps_to_VCF,
        ['--label', tmp_label,
         '--path-to-shapeit', pytest.helpers.file('fake_resources/shapeit')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 2

    seen_command_1 = mock_Popen.call_args_list[0][0][0]
    seen_command_2 = mock_Popen.call_args_list[1][0][0]

    # Calls shapeIT for each chromosome dataset

    command_1_expected = (
        'shapeit -convert ' +
        f'--input-haps {chrom_1_label} ' +
        f'--output-vcf {chrom_1_label}.phased.vcf'
    )
    assert re.search(command_1_expected, seen_command_1)

    command_2_expected = (
        'shapeit -convert ' +
        f'--input-haps {chrom_19_label} ' +
        f'--output-vcf {chrom_19_label}.phased.vcf'
    )
    assert re.search(command_2_expected, seen_command_2)
