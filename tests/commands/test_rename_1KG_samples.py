from pathlib import Path
from os.path import isfile

import pytest

from prisky.commands.rename_1KG_samples import rename_1KG_samples, \
    make_new_sample_ids, make_samples_file


def test_rename_1KG_samples(tmp_label, cli_runner, mock_Popen):
    in_vcf = f'{tmp_label}.1KG.vcf.gz'
    pytest.helpers.touch(in_vcf)

    result = cli_runner.invoke(
        rename_1KG_samples,
        ['--label', tmp_label,
         '--path-to-bcftools', pytest.helpers.file('fake_resources/bcftools')],
        catch_exceptions=False
    )
    assert 'Error' not in result.output

    command_1 = mock_Popen.call_args_list[0][0][0]
    assert f'bcftools query --list-samples {in_vcf}' in command_1

    command_2 = mock_Popen.call_args_list[1][0][0]
    out_vcf = f'{tmp_label}.1KG.samples_renamed.vcf.gz'
    samples_file = f'{tmp_label}.samples_renaming.tsv'
    assert f'bcftools reheader --samples {samples_file} {in_vcf}' in command_2
    assert f'> {out_vcf}' in command_2


def test_make_new_sample_ids():
    sample_ids = ['HG00096', 'HG00100']
    result = make_new_sample_ids(sample_ids)
    assert result == ['HG00096-GBR-EUR', 'HG00100-GBR-EUR']


def test_make_samples_file(tmp_work_dir):
    renaming_file = tmp_work_dir/'renaming.tsv'
    make_samples_file(['Old-Name-1', 'Old-Name-2'],
                      ['New-Name-1', 'New-Name-2'],
                      renaming_file)

    assert isfile(renaming_file)

    with open(renaming_file) as f:
        content = f.read()

    assert content == 'Old-Name-1\tNew-Name-1\nOld-Name-2\tNew-Name-2\n'
