import re

import pytest

from prisky.commands import merge_datasets_fixing_strand


def test_merge_datasets_fixing_strand(tmp_work_dir, cli_runner, mock_Popen):
    # Simulate these intermediate files that will be checked by the command:
    pytest.helpers.touch(tmp_work_dir/'own.subset_to_merge.variant_ids.list')
    pytest.helpers.touch(tmp_work_dir/'own.subset_to_merge.fixed.bim')

    pytest.helpers.touch(tmp_work_dir/'own.subset_to_merge.bed')
    pytest.helpers.touch(tmp_work_dir/'own.subset_to_merge.bim')
    pytest.helpers.touch(tmp_work_dir/'own.subset_to_merge.fam')

    result = cli_runner.invoke(
        merge_datasets_fixing_strand,
        ['--prefix-own', tmp_work_dir/'own',
         '--prefix-ref', tmp_work_dir/'ref',
         '--prefix-out', tmp_work_dir/'out',
         '--path-to-plink', pytest.helpers.file('fake_resources/plink'),
         '--path-to-rscript', pytest.helpers.file('fake_resources/Rscript')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 4

    seen_command_1 = mock_Popen.call_args_list[0][0][0]
    seen_command_2 = mock_Popen.call_args_list[1][0][0]
    seen_command_3 = mock_Popen.call_args_list[2][0][0]
    seen_command_4 = mock_Popen.call_args_list[3][0][0]

    command_1_expected = (
        r'Rscript .*/fix_strand_and_filter_bim_using_reference.R ' +
        r'--prefix-own .*/own --prefix-ref .*/ref ' +
        r'--prefix-out .*/own.subset_to_merge'
    )
    assert re.search(command_1_expected, seen_command_1)

    command_2_expected = (
        r'plink --bfile .*/own ' +
        r'--extract .*/own.subset_to_merge.variant_ids.list ' +
        r'--make-bed --out .*/own.subset_to_merge'
    )
    assert re.search(command_2_expected, seen_command_2)

    command_3_expected = (
        r'mv .*/own.subset_to_merge.fixed.bim .*/own.subset_to_merge.bim'
    )
    assert re.search(command_3_expected, seen_command_3)

    command_4_expected = (
        r'plink --bfile .*/own.subset_to_merge --bmerge .*/ref ' +
        r'--extract .*/own.subset_to_merge.variant_ids.list ' +
        r'--make-bed --out .*/out'
    )
    assert re.search(command_4_expected, seen_command_4)
