from unittest.mock import Mock
import shutil

import pytest

import prisky.helpers
from prisky.commands import make_regions_file


def test_make_variants_list(tmp_label, tmp_work_dir, cli_runner, monkeypatch):
    mock_make_bed_from_rs_ids = Mock()
    monkeypatch.setattr(prisky.helpers, 'make_bed_from_rs_ids',
                        mock_make_bed_from_rs_ids)

    shutil.copy2(pytest.helpers.file('Test_Pheno.variant_ids.list'),
                 tmp_work_dir)

    result = cli_runner.invoke(
        make_regions_file,
        ['--label', tmp_label,
         '--assembly', 'GRCh37'],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    mock_make_bed_from_rs_ids.assert_called_once_with(
        rs_ids=['rs1', 'rs2', 'rs3'],
        out_path=f'{tmp_label}.GRCh37.regions.bed',
        assembly='GRCh37',
    )
