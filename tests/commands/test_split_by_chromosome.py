import re

import pytest

from prisky.commands import split_by_chromosome

def test_split_by_chromosome(tmp_label, cli_runner, mock_Popen):
    fake_bim = f'{tmp_label}.bim'
    with open(fake_bim, 'w') as f:
        f.write('1\tvariant_1\t0\t100\tA\tC\n')
        f.write('5\tvariant_5\t0\t100\tA\tC\n')
        f.write('25\tvariant_25\t0\t100\tA\tC\n')

    pytest.helpers.touch(f'{tmp_label}.bed')
    pytest.helpers.touch(f'{tmp_label}.fam')

    result = cli_runner.invoke(
        split_by_chromosome,
        ['--label', tmp_label,
         '--path-to-plink', pytest.helpers.file('fake_resources/plink')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 4

    seen_command_1 = mock_Popen.call_args_list[0][0][0]
    seen_command_2 = mock_Popen.call_args_list[1][0][0]
    seen_command_3 = mock_Popen.call_args_list[2][0][0]
    seen_command_4 = mock_Popen.call_args_list[3][0][0]

    # Creates a directory for the chromosome files
    chrom_dir = tmp_label.parent/f'{tmp_label.name}_chrom_files'

    command_1_expected = (rf'mkdir -p {chrom_dir}')
    assert re.search(command_1_expected, seen_command_1)

    # Calls PLINK for each chromosome
    command_2_expected = (
        rf'plink --bfile {tmp_label} --chr 1 ' +
        rf'--make-bed --out {chrom_dir}/{tmp_label.name}.chr1'
    )
    assert re.search(command_2_expected, seen_command_2)

    command_3_expected = (
        rf'plink --bfile {tmp_label} --chr 5 ' +
        rf'--make-bed --out {chrom_dir}/{tmp_label.name}.chr5'
    )
    assert re.search(command_3_expected, seen_command_3)

    command_4_expected = (
        rf'plink --bfile {tmp_label} --chr 25 ' +
        rf'--make-bed --out {chrom_dir}/{tmp_label.name}.chr25'
    )
    assert re.search(command_4_expected, seen_command_4)
