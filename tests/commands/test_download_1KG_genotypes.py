import re

import pytest

from prisky.commands import download_1KG_genotypes


def test_download_1KG_genotypes(tmp_label, cli_runner, mock_Popen):
    regions_file = f'{tmp_label}.GRCh37.regions.bed'
    pytest.helpers.touch(regions_file)

    result = cli_runner.invoke(
        download_1KG_genotypes,
        ['--label', tmp_label,
         '--assembly', 'GRCh37',
         '--path-to-tabix', pytest.helpers.file('fake_resources/tabix'),
         '--path-to-bcftools', pytest.helpers.file('fake_resources/bcftools'),
         '--path-to-bgzip', pytest.helpers.file('fake_resources/bgzip'),
         '--path-to-java', pytest.helpers.file('fake_resources/java'),
         '--path-to-gatk3', pytest.helpers.file('fake_resources/gatk3'),
         '--path-to-reference-fasta',
         pytest.helpers.file('fake_resources/reference.fasta')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    out = f'{tmp_label}.1KG'
    command_parts = (rf'bed_to_tabix --in {regions_file} ',
                     rf'--outlabel {out} --remove-SVs --threads 24 ',
                     rf'--path-to-tabix .+/fake_resources/tabix ',
                     rf'--path-to-bcftools .+/fake_resources/bcftools ',
                     rf'--path-to-bgzip .+/fake_resources/bgzip ',
                     rf'--path-to-java .+/fake_resources/java ',
                     rf'--path-to-gatk3 .+/fake_resources/gatk3 ',
                     rf'--path-to-reference-fasta .+/fake_resources/reference')

    assert mock_Popen.call_count == 1
    for command_part in command_parts:
        assert re.search(command_part, mock_Popen.call_args[0][0])
