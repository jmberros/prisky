import os
import re

import pytest

from prisky.commands import pre_phasing_strand_check

def test_pre_phasing_strand_check(tmp_label, cli_runner, mock_Popen):
    chrom_dir = tmp_label.parent/f'{tmp_label.name}_chrom_files'
    prefix = tmp_label.name

    chrom_1_label = chrom_dir/f'{prefix}.chr1'
    chrom_19_label = chrom_dir/f'{prefix}.chr19'

    # The command checks which chrom datasets are present in the chrom dir:
    chrom_dir.mkdir()
    pytest.helpers.touch(f'{chrom_1_label}.bed')
    pytest.helpers.touch(f'{chrom_19_label}.bed')

    result = cli_runner.invoke(
        pre_phasing_strand_check,
        ['--label', tmp_label,
         '--genetic-maps-dir',  '/path/to/genetic_maps',
         '--reference-dir', '/path/to/reference',
         '--path-to-shapeit', pytest.helpers.file('fake_resources/shapeit')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    # Writes one shapeIT --check command for each chromosome dataset:

    commands_file = tmp_label.parent/f'{prefix}.pre-phasing-strand-check_commands.list'
    assert os.path.isfile(commands_file)

    with open(commands_file) as f:
        commands = [l.strip() for l in f]

    assert len(commands) == 2

    command_1_expected = (
        'shapeit -check ' +
        f'--input-bed {chrom_1_label} ' +
        '--input-map /path/to/genetic_maps/genetic_map_chr1_combined_b37.txt ' +
        '--input-ref ' +
            '/path/to/reference/1000GP_Phase3_chr1.hap.gz ' +
            '/path/to/reference/1000GP_Phase3_chr1.legend.gz ' +
            '/path/to/reference/1000GP_Phase3.sample ' +
        f'--output-log {chrom_1_label}.alignments'
    )
    assert re.search(command_1_expected, commands[0])

    command_2_expected = (
        'shapeit -check ' +
        f'--input-bed {chrom_19_label} ' +
        '--input-map /path/to/genetic_maps/genetic_map_chr19_combined_b37.txt ' +
        '--input-ref ' +
            '/path/to/reference/1000GP_Phase3_chr19.hap.gz ' +
            '/path/to/reference/1000GP_Phase3_chr19.legend.gz ' +
            '/path/to/reference/1000GP_Phase3.sample ' +
        f'--output-log {chrom_19_label}.alignments'
    )
    assert re.search(command_2_expected, commands[1])

    # Only one GNU parallel call for the commands file:
    assert mock_Popen.call_count == 1

    called_command_1 = mock_Popen.call_args_list[0][0][0]
    command_1_expected = (
        f'parallel --jobs 2 --verbose -- < {commands_file}'
    )
    assert re.search(command_1_expected, called_command_1)
