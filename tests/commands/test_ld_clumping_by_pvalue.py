import pytest

from prisky.commands.ld_clumping_by_pvalue import clump_by_pvalue


def test_clump_by_pvalue(tmp_label, mock_Popen, cli_runner):
    pytest.helpers.touch(f'{tmp_label}.bed')
    pytest.helpers.touch(f'{tmp_label}.bim')
    pytest.helpers.touch(f'{tmp_label}.fam')

    result = cli_runner.invoke(
        clump_by_pvalue,
        ['--label', tmp_label,
         '--clump-p1', 0.003,
         '--clump-p2', 0.04,
         '--clump-field', 'p_field',
         '--clump-snp-field', 'snp_field',
         '--clump-kb', 240,
         '--clump-r2', 0.4,
         '--path-to-plink', pytest.helpers.file('fake_resources/plink')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 5

    command_1 = mock_Popen.call_args_list[0][0][0]
    assert f'plink --bfile {tmp_label}' in command_1
    assert f'--clump {tmp_label}.tsv' in command_1
    assert '--clump-field p_field --clump-snp-field snp_field' in command_1
    assert '--clump-p1 0.003 --clump-p2 0.04' in command_1
    assert '--clump-kb 240 --clump-r2 0.4' in command_1
    assert f'--out {tmp_label}' in command_1

    command_2 = mock_Popen.call_args_list[1][0][0]
    assert command_2 == \
        f"awk '{{ print $3 }}' {tmp_label}.clumped | grep -E '^rs[0-9]+$' > " + \
        f"{tmp_label}.prune.in"

    command_3 = mock_Popen.call_args_list[2][0][0]
    assert f'plink --bfile {tmp_label}' in command_3
    assert f'--extract {tmp_label}.prune.in' in command_3
    assert f'--out {tmp_label}.ld-pruned' in command_3

    command_4 = mock_Popen.call_args_list[3][0][0]
    assert f'head -n1 {tmp_label}.tsv > {tmp_label}.ld-pruned.tsv' in command_4

    command_5 = mock_Popen.call_args_list[4][0][0]
    assert f'grep -wf {tmp_label}.prune.in {tmp_label}.tsv >> ' + \
        f'{tmp_label}.ld-pruned.tsv' in command_5
