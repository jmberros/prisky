from os.path import isfile

from prisky.commands import make_variants_list


def test_make_variants_list(tmp_label, cli_runner):
    result = cli_runner.invoke(
        make_variants_list,
        ['--label', tmp_label],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    out_path = f'{tmp_label}.variant_ids.list'

    assert isfile(out_path)
    with open(out_path) as f:
        content = f.read()
    assert content == 'rs1\nrs2\nrs3\n'
