import pytest

from prisky.commands import compute_genetic_score


def test_compute_genetic_score(tmp_label, cli_runner, mock_Popen):
    pytest.helpers.touch(f'{tmp_label}.bed')
    pytest.helpers.touch(f'{tmp_label}.bim')
    pytest.helpers.touch(f'{tmp_label}.fam')

    basic_scores_file = f'{tmp_label}.basic.scores.tsv'
    adj_scores_file = f'{tmp_label}.adj.scores.tsv'

    pytest.helpers.touch(basic_scores_file)
    pytest.helpers.touch(adj_scores_file)

    result = cli_runner.invoke(
        compute_genetic_score,
        ['--label', tmp_label,
         '--path-to-plink', pytest.helpers.file('fake_resources/plink')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 2

    seen_command_1 = mock_Popen.call_args_list[0][0][0]
    seen_command_2 = mock_Popen.call_args_list[1][0][0]

    expected_command_parts = [
        f'--score {adj_scores_file} sum',
        f'--score {basic_scores_file} sum',
        f'--out {tmp_label}.adj',
        f'--out {tmp_label}.basic',
    ]
    for command_part in expected_command_parts:
        assert command_part in seen_command_1 or command_part in seen_command_2

    assert f'plink --bfile {tmp_label}' in seen_command_1
    assert f'plink --bfile {tmp_label}' in seen_command_2
