import re
import os

import pytest

from prisky.commands import phase_by_chromosome

def test_phase_by_chromosome(tmp_label, cli_runner, mock_Popen):
    chrom_dir = tmp_label.parent/f'{tmp_label.name}_chrom_files'
    prefix = tmp_label.name

    chrom_1_label = chrom_dir/f'{prefix}.chr1'
    chrom_19_label = chrom_dir/f'{prefix}.chr19'

    # The command checks which chrom datasets are present in the chrom dir:
    chrom_dir.mkdir()
    pytest.helpers.touch(f'{chrom_1_label}.bed')
    pytest.helpers.touch(f'{chrom_19_label}.bed')

    # Input from the previous strand check
    pytest.helpers.touch(f'{chrom_1_label}.alignments.snp.strand.exclude')
    pytest.helpers.touch(f'{chrom_19_label}.alignments.snp.strand.exclude')

    result = cli_runner.invoke(
        phase_by_chromosome,
        ['--label', tmp_label,
         '--genetic-maps-dir',  '/path/to/genetic_maps',
         '--reference-dir', '/path/to/reference',
         '--threads', 30,
         '--extra-args', '--foo 1 --bar 2',
         '--path-to-shapeit', pytest.helpers.file('fake_resources/shapeit')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 1

    # Writes a shapeIT command for each chromosome dataset
    commands_file = tmp_label.parent/f'{prefix}.shapeIT_commands.list'
    assert os.path.isfile(commands_file)

    with open(commands_file) as f:
        commands = [l.strip() for l in f]

    assert len(commands) == 2

    # Test the content of the commands:

    command_1_expected = (
        'shapeit ' +
        f'--input-bed {chrom_1_label} ' +
        '--input-map /path/to/genetic_maps/genetic_map_chr1_combined_b37.txt ' +
        '--input-ref ' +
            '/path/to/reference/1000GP_Phase3_chr1.hap.gz ' +
            '/path/to/reference/1000GP_Phase3_chr1.legend.gz ' +
            '/path/to/reference/1000GP_Phase3.sample ' +
        f'--exclude-snp {chrom_1_label}.alignments.snp.strand.exclude ' +
        f'--output-max {chrom_1_label} ' +
        f'--output-log {chrom_1_label}.shapeit.log ' +
        f'--thread 15 ' +
        '--foo 1 --bar 2'
    )
    assert re.search(command_1_expected, commands[0])

    command_2_expected = (
        'shapeit ' +
        f'--input-bed {chrom_19_label} ' +
        '--input-map /path/to/genetic_maps/genetic_map_chr19_combined_b37.txt ' +
        '--input-ref ' +
            '/path/to/reference/1000GP_Phase3_chr19.hap.gz ' +
            '/path/to/reference/1000GP_Phase3_chr19.legend.gz ' +
            '/path/to/reference/1000GP_Phase3.sample ' +
        f'--exclude-snp {chrom_19_label}.alignments.snp.strand.exclude ' +
        f'--output-max {chrom_19_label} ' +
        f'--output-log {chrom_19_label}.shapeit.log ' +
        f'--thread 15 ' +
        '--foo 1 --bar 2'
    )
    assert re.search(command_2_expected, commands[1])

    # One GNU parallel call:

    called_command_1 = mock_Popen.call_args_list[0][0][0]
    command_1_expected = (
        f'parallel --jobs 2 --verbose -- < {commands_file}'
    )
    assert re.search(command_1_expected, called_command_1)
