import pytest

from prisky.commands import filter_by_call_rate


def test_filter_by_call_rate(tmp_label, cli_runner, mock_Popen):
    pytest.helpers.touch(f'{tmp_label}.bed')
    pytest.helpers.touch(f'{tmp_label}.bim')
    pytest.helpers.touch(f'{tmp_label}.fam')

    result = cli_runner.invoke(
        filter_by_call_rate,
        ['--label', tmp_label,
         '--geno', 0.1,
         '--mind', 0.1,
         '--path-to-plink', pytest.helpers.file('fake_resources/plink')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 4

    out_label = f'{tmp_label}.rate-filtered'

    command_1 = mock_Popen.call_args_list[0][0][0]
    assert f'plink --bfile {tmp_label}' in command_1
    assert '--mind 0.1 --geno 0.1' in command_1
    assert f'--out {out_label}' in command_1

    command_2 = mock_Popen.call_args_list[1][0][0]
    assert f"awk '{{ print $2 }}' {out_label}.bim > {out_label}.variants_list" \
        in command_2

    command_3 = mock_Popen.call_args_list[2][0][0]
    assert f'head -n1 {tmp_label}.tsv > {out_label}.tsv' in command_3

    command_4 = mock_Popen.call_args_list[3][0][0]
    assert f'grep -wf {out_label}.variants_list {tmp_label}.tsv >> {out_label}.tsv' in command_4
