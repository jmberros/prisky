from pathlib import Path

from prisky.commands import make_score_files


def test_make_score_files(tmp_label, cli_runner, caplog):
    result = cli_runner.invoke(
        make_score_files,
        ['--label', tmp_label],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output
    assert 'basic_score' in caplog.text
    assert 'adjusted_score' in caplog.text

    out = Path(f'{tmp_label}.basic.scores.tsv')
    assert out.is_file()
    assert out.read_text() == 'rs1\tA\t1.5\nrs2\tT\t-0.5\nrs3\tC\t3.0\n'

    out = Path(f'{tmp_label}.adjusted.scores.tsv')
    assert out.is_file()
    assert out.read_text() == 'rs1\tA\t1.3\nrs2\tT\t-0.3\nrs3\tC\t3.0\n'

    out = Path(f'{tmp_label}.basic.score_quantiles.tsv')
    assert out.is_file()
    assert out.read_text().split('\n') == [
        'variant_id\tbasic_score\tbasic_score_quantile',
        'rs1\t1.5\t0.66667',
        'rs2\t-0.5\t1.0',
        'rs3\t3.0\t0.33333',
        ''
    ]

    out = Path(f'{tmp_label}.adjusted.score_quantiles.tsv')
    assert out.is_file()
    assert out.read_text().split('\n') == [
        'variant_id\tadjusted_score\tadjusted_score_quantile',
        'rs1\t1.3\t0.66667',
        'rs2\t-0.3\t1.0',
        'rs3\t3.0\t0.33333',
        ''
    ]
