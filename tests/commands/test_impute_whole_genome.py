import os
import re

import pytest

from prisky.commands import impute_whole_genome


def test_impute_whole_genome(tmp_label, cli_runner, mock_Popen):
    prefix = tmp_label.name
    chrom_dir = tmp_label.parent/f'{prefix}_chrom_files'
    impute_dir = tmp_label.parent/f'{prefix}_impute2_foobar'

    chrom_1_label = chrom_dir/f'{prefix}.chr1'
    chrom_19_label = chrom_dir/f'{prefix}.chr19'

    # Input from shapeIT, chromosome {haps,sample} files:
    chrom_dir.mkdir()
    pytest.helpers.touch(f'{chrom_1_label}.haps')
    pytest.helpers.touch(f'{chrom_19_label}.haps')

    result = cli_runner.invoke(
        impute_whole_genome,
        ['--label', tmp_label,
         '--genetic-maps-dir',  '/path/to/genetic_maps',
         '--reference-dir', '/path/to/reference',
         '--threads', 3,
         '--extra-args', '--foo 1 --bar 2',
         '--out-suffix', 'foobar',
         '--path-to-impute2', pytest.helpers.file('fake_resources/impute2'),
         '--path-to-parallel', pytest.helpers.file('fake_resources/parallel')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    commands_file = tmp_label.parent/f'{prefix}.impute2_foobar_commands.list'

    # Check the commands written to the commands_file are correct
    # - 50 impute calls for chrom 1, because it's 5Mb each call, up to 250Mb
    # - 13 impute calls for chrom 19, up to 65Mb total chrom length

    assert os.path.isfile(commands_file)

    with open(commands_file) as f:
        commands = [l.strip() for l in f]

    assert len(commands) == 50 + 13

    # I will test only a couple of calls, not the 63

    command_2_expected = (
        'impute2 ' +
        '-m /path/to/genetic_maps/genetic_map_chr1_combined_b37.txt ' +
        f'-known_haps_g {chrom_1_label}.haps ' +
        '-h /path/to/reference/1000GP_Phase3_chr1.hap.gz ' +
        '-l /path/to/reference/1000GP_Phase3_chr1.legend.gz ' +
        '-Ne 20000 ' +
        '-int 5000001 10000000 ' +
        f'-o {impute_dir}/{prefix}.chr1.chunk02.5000001-10000000.gen ' +
        f'-i {impute_dir}/{prefix}.chr1.chunk02.5000001-10000000.impute2_info ' +
        f'-r {impute_dir}/{prefix}.chr1.chunk02.5000001-10000000.impute2_summary ' +
        f'-w {impute_dir}/{prefix}.chr1.chunk02.5000001-10000000.impute2_warnings ' +
        '--foo 1 --bar 2'
    )
    assert re.search(command_2_expected, commands[1])

    command_63_expected = (
        'impute2 ' +
        '-m /path/to/genetic_maps/genetic_map_chr19_combined_b37.txt ' +
        f'-known_haps_g {chrom_19_label}.haps ' +
        '-h /path/to/reference/1000GP_Phase3_chr19.hap.gz ' +
        '-l /path/to/reference/1000GP_Phase3_chr19.legend.gz ' +
        '-Ne 20000 ' +
        '-int 60000001 65000000 ' +
        f'-o {impute_dir}/{prefix}.chr19.chunk13.60000001-65000000.gen ' +
        f'-i {impute_dir}/{prefix}.chr19.chunk13.60000001-65000000.impute2_info ' +
        f'-r {impute_dir}/{prefix}.chr19.chunk13.60000001-65000000.impute2_summary ' +
        f'-w {impute_dir}/{prefix}.chr19.chunk13.60000001-65000000.impute2_warnings ' +
        '--foo 1 --bar 2'
    )
    assert re.search(command_63_expected, commands[62])

    # Only one GNU parallel call for the commands file:
    assert mock_Popen.call_count == 1

    called_command_1 = mock_Popen.call_args_list[0][0][0]
    command_1_expected = (
        f'parallel --jobs 3 --verbose -- < {commands_file}'
    )
    assert re.search(command_1_expected, called_command_1)
