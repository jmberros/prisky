import re

import pytest

from prisky.commands import merge_impute2_chunk_files


def test_merge_impute2_chunk_files(tmp_label, cli_runner, mock_Popen):
    result = cli_runner.invoke(
        merge_impute2_chunk_files,
        ['--label', tmp_label,
         '--threads', 4,
         '--out-suffix', 'foo_bar',
         '--path-to-parallel', pytest.helpers.file('fake_resources/parallel'),
         '--path-to-qctool', pytest.helpers.file('fake_resources/qctool')],
        catch_exceptions=False,
    )
    assert 'Error' not in result.output

    assert mock_Popen.call_count == 4

    called_command_1 = mock_Popen.call_args_list[0][0][0]
    called_command_2 = mock_Popen.call_args_list[1][0][0]
    called_command_3 = mock_Popen.call_args_list[2][0][0]
    called_command_4 = mock_Popen.call_args_list[3][0][0]

    command_1_expected = (
        r'seq 1 22 \| ' +
        r'.*/parallel ' +
        r"'" + # parallel command begins
        r'head --silent -n1 ' +
        r'.*/Test_Pheno_impute2_foo_bar/Test_Pheno.chr{}.chunk\*.impute2_info' +
        r"' " # parallel command ends
        r'\| uniq | tee .*/Test_Pheno.foo_bar.impute2_info'
    )
    assert re.search(command_1_expected, called_command_1)

    command_2_expected = (
        r'seq 1 22 \| ' +
        r'.*/parallel --verbose --keep-order ' +
        r"'" + # parallel command begins
        r'tail --silent -n\+2 ' +
        r'.*/Test_Pheno_impute2_foo_bar/Test_Pheno.chr{}.chunk\*.impute2_info ' +
        r'\| sed s/\^---/{}/' +
        r"' " # + # parallel command ends
        r'>> .*/Test_Pheno.foo_bar.impute2_info'
    )
    assert re.search(command_2_expected, called_command_2)

    command_3_expected = (
        r'seq 1 22 \| ' +
        r'.*/parallel --verbose --jobs 4 ' +
        r"'" + # parallel command surrounded by single quotes
        r'cat .*/Test_Pheno_impute2_foo_bar/Test_Pheno.chr{}.chunk\*.gen ' +
        r'> .*/Test_Pheno_impute2_foo_bar/Test_Pheno.chr{}.gen' +
        r"'" # parallel command surrounded by single quotes
    )
    assert re.search(command_3_expected, called_command_3)

    command_4_expected = (
        r'.*/qctool ' +
        r'-g .*/Test_Pheno_impute2_foo_bar/Test_Pheno.chr#.gen ' +
        r'-og .*/Test_Pheno.foo_bar.bgen'
    )
    assert re.search(command_4_expected, called_command_4)
