import subprocess
from unittest.mock import Mock

from prisky.helpers import run_shell_command


class FakeStdout:
    def __init__(self, command):
        self.lines = [f'{command} first line', f'{command} second line', '']
        self.lines = [line.encode('utf-8') for line in self.lines]

    def readline(self):
        return self.lines.pop(0)


def test_run_shell_command(monkeypatch, caplog):
    mock_process = Mock()
    mock_process.poll = Mock(return_value=True)
    mock_Popen = Mock(return_value=mock_process)
    monkeypatch.setattr(subprocess, 'Popen', mock_Popen)

    mock_process.stdout = FakeStdout('command-1')
    result = run_shell_command('command-1')

    assert mock_Popen.call_count == 1
    assert mock_Popen.call_args[0][0] == 'command-1'
    assert mock_Popen.call_args[1]['shell'] == True

    assert result == ['command-1 first line', 'command-1 second line']

    assert 'Running:' in caplog.text
    assert 'command-1 first line' in caplog.text
    assert 'command-1 second line' in caplog.text

    mock_process.stdout = FakeStdout('command-2')
    result = run_shell_command('command-2', silence_stdout=True)

    assert result == ['command-2 first line', 'command-2 second line']

    assert 'command-2 first line' not in caplog.text
    assert 'command-2 second line' not in caplog.text
