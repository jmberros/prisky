import re
from os.path import isdir
from pathlib import Path

import pytest

from prisky.helpers.common import (
    abs_label,
    get_1KG_samples,
    read_variants,
    check_output,
    check_input,
    InputMissingException,
    EmptyInputException,
    package_root,
    chrom_dir_for_label,
    find_chromosome_datasets,
    chrom_lengths,
    chrom_chunks,
    write_commands_to_file,
    make_Path,
    add_leading_underscore_if_present,
)


def test_make_Path():
    result = make_Path(None, None, value = '/foo/bar')
    assert isinstance(result, Path)
    assert str(result) == '/foo/bar'


def test_add_leading_underscore_if_present():
    result = add_leading_underscore_if_present(None, None, value = 'foo')
    assert result == '_foo'

    result = add_leading_underscore_if_present(None, None, value = '')
    assert result == ''


def test_write_commands_to_file(tmp_path):
    fp = tmp_path/'commands.list'
    write_commands_to_file(['command-1', 'command-2'], fp)
    assert fp.read_text() == 'command-1\ncommand-2\n'


def test_find_chromosome_datasets(tmp_label):
    prefix = tmp_label.name
    chrom_dir = tmp_label.parent/f'{prefix}_chrom_files'
    chrom_dir.mkdir()
    pytest.helpers.touch(chrom_dir/f'{prefix}.chr8.bed')
    pytest.helpers.touch(chrom_dir/f'{prefix}.chrX.bed')

    result = find_chromosome_datasets(tmp_label)

    assert result == [
        ('8', str(chrom_dir/f'{prefix}.chr8')),
        ('X', str(chrom_dir/f'{prefix}.chrX'))
    ]

    result = find_chromosome_datasets(tmp_label, only_chrom='X')
    assert result == [('X', str(chrom_dir/f'{prefix}.chrX'))]

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        result = find_chromosome_datasets(tmp_label, only_chrom='Z',
                                          exit_if_zero=True)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 42

    # Check it can find shapeIT files too:

    pytest.helpers.touch(chrom_dir/f'{prefix}.chr5.haps')

    result = find_chromosome_datasets(tmp_label, extension="haps")
    assert result == [
        ('5', str(chrom_dir/f'{prefix}.chr5')),
    ]

def test_package_root():
    assert str(package_root).endswith('/prisky')
    assert isdir(package_root/'priskyr')


def test_chrom_dir_for_label():
    label = Path('/path/to/dataset')
    result = chrom_dir_for_label(label)

    assert isinstance(result, Path)
    assert str(result) == '/path/to/dataset_chrom_files'


def test_abs_label():
    result = abs_label('~/path/to/work/label-1')
    assert re.search(r'/home/.+/path/to/work/label-1', result)


def test_get_1KG_samples():
    result = get_1KG_samples()
    assert len(result) == 2504
    assert list(result.columns) == ['sample', 'pop', 'super_pop', 'gender']


def test_chrom_lengths():
    result = chrom_lengths()
    assert len(result) == 24
    assert result["10"] == 135374737


def test_chrom_chunks():
    result = chrom_chunks("10", step=10e6)
    assert len(result) == 14
    assert result[0][1] - result[0][0] == 10e6 - 1
    assert result[1][0] == 10e6 + 1
    assert result[1][1] == 20e6
    assert result[-1][0] == 130e6 + 1
    assert result[-1][1] == 140e6

    result_int = chrom_chunks(10, step=10e6)
    assert result_int == result


def test_read_variants(tmp_label):
    path = f'{tmp_label}.tsv'
    result = read_variants(path)
    assert list(result['variant_id']) == ['rs1', 'rs2', 'rs3']
    assert list(result['effect_allele']) == ['A', 'T', 'C']


def test_check_file_and_log(caplog):
    non_existent_file = 'non-existent-file'
    result = check_output(non_existent_file)
    assert result is False
    assert 'Output already exists:' not in caplog.text

    existent_file = pytest.helpers.file('Test_Pheno.tsv')
    result = check_output(existent_file)
    assert result is True
    assert 'Output already exists:' in caplog.text

def test_check_input(tmp_path):
    input_file = tmp_path/'input-file'

    with pytest.raises(InputMissingException):
        check_input(input_file)

    input_file.touch()
    with pytest.raises(EmptyInputException):
        check_input(input_file)

    # Does not raise anything:
    input_file.write_text('some content')
    assert check_input(input_file) is True
