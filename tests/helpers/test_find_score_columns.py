import pandas as pd

from prisky.helpers import find_score_columns


def test_find_score_columns(tmp_path):

    df = pd.DataFrame({
        'non_score_column': [1, 2, 3],
        'basic_score': [2, 3, 4],
        'another_score': [3, 4, 5],
    })
    expected_columns = ['another_score', 'basic_score']

    # From pandas DataFrame:

    result = find_score_columns(df)
    assert sorted(result) == expected_columns

    # From filepath:

    tsv_path = tmp_path/'foobar.tsv'
    df.to_csv(tsv_path, sep="\t")

    result = find_score_columns(tsv_path)
    assert sorted(result) == expected_columns
