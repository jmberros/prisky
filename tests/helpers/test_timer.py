import time

from prisky.helpers import Timer


def test_timing_and_logging(caplog):
    timer = Timer()
    time.sleep(0.01)
    timer.stop_and_log("Message:")
    assert 'Message: 0.01 seconds' in caplog.text

    timer.start()
    time.sleep(0.05)
    timer.stop_and_log("Second message:")
    assert 'Second message: 0.05 seconds' in caplog.text

    timer.start()
    time.sleep(0.02)
    timer.stop_and_log()
    assert 'Done! Took: 0.02 seconds' in caplog.text
