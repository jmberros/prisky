from glob import glob
from pathlib import Path

from freezegun import freeze_time

from prisky.logger import logger, set_logfile


@freeze_time('1985-04-10 09:10:00')
def test_logger_and_set_logfile(caplog, tmp_path):
    log_file_1 = tmp_path/'foo.log'
    log_file_2 = tmp_path/'bar.log'

    set_logfile(log_file_1)
    logger.info('info text 0')

    set_logfile(log_file_2)
    logger.info('info text 2')

    set_logfile(log_file_1) # Will overwrite 'info text 0'
    logger.info('info text 1')

    set_logfile(None)
    logger.info('info without logfile')

    # Find the timestamped actual log files
    expected_timestamp = '19850410-091000'
    log_file_1 = Path(tmp_path/f'foo.{expected_timestamp}.log')
    log_file_2 = Path(tmp_path/f'bar.{expected_timestamp}.log')

    # Check stuff is logged to the screen:
    assert 'info text 0' in caplog.text
    assert 'info text 1' in caplog.text
    assert 'info text 2' in caplog.text
    assert 'info without logfile' in caplog.text

    # Check stuff is logged to lof files:
    log_file_1_content = log_file_1.read_text()
    log_file_2_content = log_file_2.read_text()

    assert 'info text 1' in log_file_1_content
    assert 'info text 1' not in log_file_2_content
    assert 'info text 2' in log_file_2_content
    assert 'info text 2' not in log_file_1_content
    assert 'info without logfile' not in log_file_1_content
    assert 'info without logfile' not in log_file_2_content
