import pytest

from prisky.helpers import make_bed_from_rs_ids, raise_if_any_id_is_not_rs_id


def test_check_all_ids_are_rs_ids():
    with pytest.raises(Exception):
        raise_if_any_id_is_not_rs_id(['rs1;rs2'])

    with pytest.raises(Exception):
        raise_if_any_id_is_not_rs_id(['rs1,rs2'])

    # Does not raise anything:
    raise_if_any_id_is_not_rs_id(['rs1', 'rs2'])


# This tests performs a real annotation from the web/cache from anotala
def test_make_bed_from_rs_ids(tmpdir):
    out_path = tmpdir.join('variants.bed')
    make_bed_from_rs_ids(rs_ids=['rs268', 'rs9939609'], out_path=out_path)

    with open(out_path) as f:
        lines = f.readlines()

    assert len(lines) == 2
    assert lines[0] == '8\t19813529\t19813529\trs268\n'
    assert lines[1] == '16\t53820527\t53820527\trs9939609\n'

    from prisky.helpers.make_bed_from_rs_ids import EnsemblAnnotator
    assert EnsemblAnnotator.full_info is False
