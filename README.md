# Requirements

  - Python 3.7 or newer. You can get it from [miniconda|https://conda.io/en/latest/miniconda.html].
  - `anotala`: a Python package for variant annotation.
```bash
git clone https://github.com/biocodices/anotala
cd anotala && python setup.py install
```
  - `bed_to_tabix`: a Python package for 1KG genotypes downloading.
```bash
git clone https://github.com/biocodices/bed_to_tabix
cd bed_to_tabix && python setup.py install
```
  - PLINK
  - bcftols
  - bgzip
  - tabix
  - java 8 for GATK3
  - GATK3
  - `R` and `Rscript`. In Ubuntu 18.04 you would run: `sudo apt install gdebi libxml2-dev libssl-dev libcurl4-openssl-dev libopenblas-dev r-base r-base-dev`.
  - R packages:

```R
install.packages(c("devtools", "docopt", "tidyverse", "shiny", "ggridges", "R.utils")
library(devtools)
devtools::install_github("elbersb/tidylog")
install.packages("testthat") # For devs
```

  - We also assume that the (Unix) system where this is run has the following
    commands: `awk`, `grep`.

# Usage

Choose a label for the phenotype-panel, `Pheno-1` for instance. This label
works as PLINK labels: it should be a full path without the extension, for
instance, `/home/user/workdir/Pheno-1`, or just `Pheno-1` if you are already
working in `/home/user/workdir`.

Generate a TSV of variants for a given phenotype panel named like
`Pheno-1.tsv`. The TSV must contain these headers:

   - `variant_id`: the rs ID of the variant.
   - `basic_score`: either the estimated beta or the log(OR) of the effect allele
   - `effect_allele`: the effect allele associated to the score. This allele
     should match one of the alleles found in the VCF for this variant.
   - [`<tag>_score`]: any number of optional extra score columns labeled
     differently, for instance, `adjusted_score`, `ancestry_adj_score`, etc.

We will assume you work in a `workdir` directory that has that `Pheno-1.tsv` in
it:

```
# workdir
# |_ Pheno-1.tsv
```

Define these variables in your environment (you can put this lines in your
`~/.zsrhc` if you use prisky regularly):

```bash
export PATH_TO_BCFTOOLS=/software/bcftools-1.9/bcftools
export PATH_TO_BGZIP=/software/htslib-1.9/bgzip
export PATH_TO_TABIX=/software/htslib-1.9/tabix
export PATH_TO_GATK3=/software/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef/GenomeAnalysisTK.jar
export PATH_TO_REFERENCE_FASTA=/biocodices/paip_resources/reference/human_g1k_v37.fasta
export PATH_TO_JAVA=/usr/bin/java
export PATH_TO_PLINK=/software/plink/plink
export PATH_TO_RSCRIPT=/usr/bin/Rscript
```

If these variables are not set in the environment, they will be asked for at
each step as arguments, like `--path-to-bcftools`. You can provide them in that
way as well, but having them in the `~/.zshrc` is more practical.

## 0. Preparation step for UKBB datasets

Given one of the association files from UKBB, as downloaded from ...
The resulting TSV file works as input for the start of the pipeline.

```bash
prisky prepare-ukbb-tsv --label UKBB_Pheno

# workdir
# |_ UKBB_Pheno.1234.assoc.tsv # <-- IN
# |_ UKBB_Pheno.tsv # <-- OUT ✨
```

## 1. Make a list of the rs IDs

We begin by generating a variants list file:

```bash
prisky make-variants-list --label Pheno-1

# workdir
# |_ Pheno-1.tsv # <-- IN
# |_ Pheno-1.variant_ids.list # --> OUT ✨
```

## 2. Make a BED of regions from the rs IDs

Then we annotate the variant IDs (we assume they are rs IDs) with their
genomic coordinates and produce a regions file (BED):

```bash
prisky make-regions-file --label Pheno-1 --assembly GRCh37

# workdir
# |_ Pheno-1.variant_ids.list # <-- IN
# |_ Pheno-1.GRCh37.regions.bed # --> OUT ✨
# |_ Pheno-1.merged-sorted-expanded.GRCh37.regions.bed # --> OUT ✨
```

The `merged-sorted-expanded` BED has zero-length regiong expanded by +1 bp each
side. With this BED, we download the genotypes from 1KG at those coordinates.


## 3. Download 1KG genotypes at those coordinates

We download the genotypes from 2,504 samples from 26 populations from the
1KG Project.

`bed_to_tabix` is used for this task, and it will ask for paths to some tools
that you must have installed (tabix, bcftools, GATK3, java for GATK3, bgzip,
and a FASTA human reference).


```bash
prisky download-1KG-genotypes --label Pheno-1 --assembly GRCh37
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_BGZIP or --path-to-bgzip
#   PATH_TO_TABIX or --path-to-tabix
#   PATH_TO_JAVA or --path-to-java
#   PATH_TO_GATK3 or --path-to-gatk3
#   PATH_TO_BCFTOOLS or --path-to-bcftools
#   PATH_TO_REFERENCE_FASTA or --path-to-reference-fasta

# workdir
# |_ Pheno-1.merged-sorted-expanded.GRCh37.regions.bed # <-- IN
# |_ Pheno-1.1KG.vcf.gz # --> OUT ✨
```

The result is a VCF file.

## Rename 1KG samples to include the populations and regions

Next we modify the 1KG VCF to include the population and regions in the sample
names, for instance `HG00096` -> `HG00096-GBR-EUR`. `bcftools reheader -s`
utility is used for this task.

The result is a VCF with new sample names.

```bash
# workdir
# |_ Pheno-1.1KG.vcf.gz # <-- IN

prisky rename_1KG_samples --label Pheno-1 --assembly GRCh37
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_BCFTOOLS or --path-to-bcftools

# workdir
# |_ Pheno-1.sample_renaming.tsv # --> OUT ✨
# |_ Pheno-1.1KG.samples_renamed.vcf.gz # --> OUT ✨
```

## [PENDING] Make the final VCF for the allelic scoring

Transform the `Pheno-1.1KG.vcf.gz` in any way you want (maybe merge it to
another set of genotypes?) to produce the final VCF file that will be used
for the allele scoring: `Pheno-1.vcf.gz`.

```bash
# workdir
# |_ Pheno-1.vcf.gz # --> OUT ✨
```

## VCF to PLINK dataset

We need a PLINK dataset of `{bed,bim,fam}` files from our VCF of genotypes to
continue:

```bash
.
# workdir
# |_ Pheno-1.vcf.gz # <-- IN

prisky make-plink-dataset --label Pheno-1
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_PLINK or --path-to-plink

# workdir
# |_ Pheno-1.{bed,bim,fam} # --> OUT ✨
```

## Phasing and Imputation

The idea is to impute variants in your own dataset using 1KG samples as
reference. This takes two PLINK datasets as input, not VCFs.

### Split dataset by chromosome

```bash
# workdir
# |_ Pheno-1.{bed,bim,fam} # <-- IN

prisky split-by-chromosome --label Pheno-1
  ## Also needs:
  # --path-to-plink /path/to/plink

# workdir
# |_ Pheno-1_chrom_files <-- OUT ✨
#   |_ Pheno-1.chr1.{bed,bim,fam}
#   |_ Pheno-1.chr2.{bed,bim,fam}
#   |_ ...
```

### Strand check before phasing with shapeIT

```bash
# workdir
# |_ Pheno-1_chrom_files <--IN
#   |_ Pheno-1.chr1.{bed,bim,fam}
#   |_ Pheno-1.chr2.{bed,bim,fam}
#   |_ ...

# /path/to/genetic_maps_dir
# |_ genetic_map_chr<N>_combined_b37.txt

# /path/to/reference-dir
# |_ 1000GP_Phase3.sample
# |_ 1000GP_Phase3_chr<N>.hap.gz
# |_ 1000GP_Phase3_chr<N>.legend.gz

prisky pre-phasing-strand-check --label Pheno-1 \
  --genetic-maps-dir /path/to/genetic_maps_dir \
  --reference-dir /path/to/haplotype_references_dir
  ## Also needs:
  # --path-to-shapeit /path/to/shapeit

# workdir
# |_ Pheno-1_chrom_files <--IN
#   |_ Pheno-1.chr1.alignments.snp.strand.exclude <-- OUT ✨
#   |_ ...
```

### Phase by chromosomes using shapeIT

This step phases the chromosome datasets using the `snp.strand.exclude` files
to remove SNPs with alignment issues.

```bash
# workdir
# |_ Pheno-1_chrom_files <-- IN
#   |_ Pheno-1.chr1.{bed,bim,fam}
#   |_ Pheno-1.chr1.alignments.snp.strand.exclude
#   |_ Pheno-1.chr2.{bed,bim,fam}
#   |_ Pheno-1.chr2.alignments.snp.strand.exclude
#   |_ ...

prisky phase-by-chromosome --label Pheno-1 \
  --genetic-maps-dir /path/to/genetic_maps_dir \
  --reference-dir /path/to/haplotype_references_dir \
  ## Optional arguments:
  --threads 8 \
  --extra-args "--window 0.5 --states 200"  \
  --only-chrom 5
  ## Also needs:
  # --path-to-shapeit /path/to/shapeit # or PATH_TO_SHAPEIT

# workdir
# |_ Pheno-1_chrom_files
#   |_ Pheno-1.chr1.{haps,sample} <-- OUT ✨
#   |_ ...
```

### Impute

```bash
# workdir
# |_ Pheno-1_chrom_files
#   |_ Pheno-1.chr1.{haps,sample} <-- IN
#   |_ ...

prisky impute-whole-genome --label Pheno-1 \
  --genetic-maps-dir /path/to/genetic_maps_dir \
  --reference-dir /path/to/haplotype_references_dir \
  ## Optional arguments:
  --threads 8 \
  --extra-args "--window 0.5 --states 200"  \
  --only-chrom 5
  ## Also needs:
  # --path-to-impute2 /path/to/impute2  # or PATH_TO_IMPUTE2
  # --path-to-parallel /path/to/GNU-parallel # or PATH_TO_PARALLEL

# workdir
# |_ Pheno-1_impute2 <-- OUT ✨
#   |_ Pheno-1.chr1.chunk01.1-5000000.gen
#   |_ Pheno-1.chr1.chunk01.1-5000000.impute2{_summary,_warnings,_info,...}
#   |_ Pheno-1.chr1.chunk02.5000001-1000000.gen
#   |_ Pheno-1.chr1.chunk02.5000001-1000000.impute2{_summary,_warnings,_info,...}
#   |_ ...
```

Since the imputation is run in chunks and in parallel, the output is only
printed when each chunk finishes. If you want to follow the output in real
time, you can tail the impute output files while it's running

```bash
tail -n0 -f Pheno-1_impute2/*summary*
```

#### Merge impute2 .gen files

The following command produces a single `.gen` and `impute2_info` pair of files
for chromosome, so you don't have to deal with 500+ chunk files.

```bash
# workdir
# |_ Pheno-1_impute2 <-- IN
#   |_ Pheno-1.chr1.chunk01.1-5000000.gen
#   |_ Pheno-1.chr1.chunk01.1-5000000.impute2{_summary,_warnings,_info,...}
#   |_ Pheno-1.chr1.chunk02.5000001-1000000.gen
#   |_ Pheno-1.chr1.chunk02.5000001-1000000.impute2{_summary,_warnings,_info,...}
#   |_ ...

prisky merge-impute2-gen-files --label Pheno-1

# workdir
# |_ Pheno-1_impute2.bgen <--  OUT ✨
# |_ Pheno-1_impute2.impute2_info <--  OUT ✨
```

### Convert {haps,sample} sets to VCF

And now we need to convert the files to VCF:

```bash
# workdir
# |_ Pheno-1_chrom_files
#   |_ Pheno-1.chr1.{haps,sample} <-- IN
#   |_ ...

prisky convert-haps-to-VCF --label Pheno-1

# workdir
# |_ Pheno-1_chrom_files
#   |_ Pheno-1.chr1.phased.vcf <-- OUT ✨
#   |_ ...
```

### Merge your dataset with the 1KG reference

This script merges your plink dataset with the 1KG reference dataset, fixing
the strand of variants when possible, or leaving out variants if this fix
cannot be done.

```bash
# workdir
# |_ Pheno-1.MySamples.{bed,bim,fam} # <-- IN
# |_ Pheno-1.1KG.{bed,bim,fam} # <-- IN

prisky merge-datasets-fixing-strand \
  --prefix-own Pheno-1.MySamples \
  --prefix-ref Pheno-1.1KG \
  --prefix-out Pheno-1.MySamples-1KG-merged

# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_RSCRIPT or --path-to-rscript
#   PATH_TO_PLINK or --path-to-plink

# workdir
# |_ Pheno-1.MySamples.subset_to_merge.variant_ids.list # --> OUT ✨
# |_ Pheno-1.MySamples.subset_to_merge.{bed,bim,fam} # --> OUT ✨
# |_ Pheno-1.MySamples-1KG-merged.{bed,bim,fam} # --> OUT ✨
```

## Allelic scoring

We use PLINK to compute the genetic score per sample or "profile".
Then we make "scores" files (one per `_score` column in the TSV) keeping only
the scores per variant/allele, in the format PLINK `--score` expects.

We also make "score quantiles" files (one per `_score` column again),
in the format PLINK `--clump` expects (but with custom column names), where
the quantile is based on the _absolute value_ of the effect size.

```bash
prisky make-score-files --label Pheno-1
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_PLINK or --path-to-plink

# workdir
# |_ Pheno-1.tsv # <-- IN
# |_ Pheno-1.basic.scores.tsv # --> OUT ✨
# |_ Pheno-1.basic.score_quantiles.tsv # --> OUT ✨
# |_ Pheno-1.adjusted.scores.tsv # --> OUT ✨
# |_ Pheno-1.adjusted.score_quantiles.tsv # --> OUT ✨
```

## Clump variants in linkage disequilibrium

The variants need to be pruned by LD before computing the polygenic score, so
there is no double counting when many alleles of the same region signal the
same causal variant. From each group of alleles in disequilibirum, we choose
the one with the lowest p-value (that is, the variant with the most significant
association with the phenotype).

Clump the variants in linkage disequilibrium and choose the most significant
one (the one with the lowest p-value) from each group:

```bash
prisky clump-by-pvalue --label Pheno-1
# This command accepts some extra parameters, with these default values:
#
#   --clump-snp-field variant_id
#   --clump-field p_value
#   --clump-p1 0.0001
#   --clump-p2 0.01
#   --clump-kb 250
#   --clump-r2 0.5
#
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_PLINK or --path-to-plink

# workdir
# |_ Pheno-1.{bed,bim,fam} # <-- IN
# |_ Pheno-1.clumped # <-- OUT ✨
# |_ Pheno-1.ld-pruned.{bed,bim,fam} # <-- OUT ✨
# |_ Pheno-1.ld-pruned.tsv # <-- OUT ✨
```

The new TSV file has the independent variants and their extra data from the
original TSV file of variants.

From this point, the pipeline should continue with the `Pheno-1.ld-pruned`
label. However, the commands can be run with any `--label` argument, in
case any of the analyses needs to be run on the full set of variants.

## Compute the polygenic score

Finally, we use PLINK to compute the genetic score per sample, and per scores
file:

```bash
prisky compute-genetic-score --label Pheno-1
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_PLINK or --path-to-plink

# workdir
# |_ Pheno-1.ld-pruned.{bed,bim,fam} # <-- IN
# |_ Pheno-1.ld-pruned.basic.profile # <-- OUT ✨
# |_ Pheno-1.ld-pruned.adjusted.profile # <-- OUT ✨
```

## Analyze the effect sizes

Now we use R to plot the distribution of each score in our set:

```bash
prisky analyze-scores --label Pheno-1
# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_RSCRIPT or --path-to-rscript

# workdir
# |_ Pheno-1.ld-pruned.tsv # <-- IN
# |_ Pheno-1.ld-pruned.basic.png # <-- OUT ✨
# |_ Pheno-1.ld-pruned.adjusted.png # <-- OUT ✨
```

## Plot the scores

We now plot the computed scores per sample/population.

```bash
prisky plot-scores --label Pheno-1 --score-tag basic

# Also uses these ENV variables or, if not defined, these extra parameters:
#
#   PATH_TO_RSCRIPT or --path-to-rscript

# workdir
# |_ Pheno-1.ld-pruned.basic.profile # <-- IN

# OUT: a Shiny server running to explore the scores
```

# Development

Python code is tested with `pytest`:

```bash
git clone git@gitlab.com:jmberros/prisky
cd prisky
python setup.py develop
pytest
```

R code inside `prisky/priskyr` has tests as well. In RStudio, open the project
at `prisky/priskyr/priskyr.Rproj` and hit `Ctrl+Shift+T`.
